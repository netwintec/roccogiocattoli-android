﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Webkit;
using RestSharp;

namespace RoccoGiocattoli
{
	[Activity (Label = "Notification",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]		
	public class NotificationActivity : BaseActivity
	{

		ImageView menuIcon;
		ImageView backIcon;

		ImageView image;
		TextView description;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			MainActivity.Instance.Scan = false;

			bool beacon = Intent.GetBooleanExtra ("Beacons",false);
			bool notification = Intent.GetBooleanExtra ("Notification",false);

			Console.WriteLine ("FLAG:" + beacon + "|" + notification);
			ISharedPreferences prefs = MainActivity.Instance.prefs;	
			if(beacon){
				//****** CHIAMATA BEACON VISTO CON TOKEN
				var client = new RestClient("http://api.netwintec.com:81/");
				//client.Authenticator = new HttpBasicAuthenticator(username, password);

				Console.WriteLine ("LETTO BEACON :"+"notification/beacon/"+MainActivity.Instance.Key);

				var requestN4U = new RestRequest("notification/beacon/"+MainActivity.Instance.Key, Method.GET);
				requestN4U.AddHeader("content-type", "application/json");
				requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
				requestN4U.AddHeader ("Net4U-Token",prefs.GetString ("TokenN4U", null));
				requestN4U.Timeout = 60000;

				Console.WriteLine ("1");
				IRestResponse aa = client.Execute(requestN4U);
				Console.WriteLine ("2 "+ aa.StatusCode+"|"+aa.Content);
			}
			if(notification){
				
				//****** CHIAMATA PUSH VISTA SENZA TOKEN
				var client = new RestClient("http://api.netwintec.com:81/");
				//client.Authenticator = new HttpBasicAuthenticator(username, password);

				Console.WriteLine ("LETTO PUSH :"+"notification/push/"+MainActivity.Instance.PushKey);

				var requestN4U = new RestRequest("notification/push/"+MainActivity.Instance.PushKey, Method.GET);
				requestN4U.AddHeader("content-type", "application/json");
				requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
				//requestN4U.AddHeader ("Net4U-Token",prefs.GetString ("TokenN4U", null));
				requestN4U.Timeout = 60000;

				Console.WriteLine ("1");
				client.Execute(requestN4U);
				Console.WriteLine ("2");

			}

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);

			setActionBarIcon(Resource.Drawable.menu_toolbar,Resource.Drawable.back);

			menuIcon.Visibility = ViewStates.Invisible;
			backIcon.Visibility = ViewStates.Visible;

			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

			backIcon.Click += delegate {
				MainActivity.Instance.Scan = true;
				Finish();

			};

			Typeface Roboto = Typeface.CreateFromAsset(Assets,"fonts/Roboto_Regular.ttf");

			image = FindViewById <ImageView> (Resource.Id.image);
			description = FindViewById <TextView> (Resource.Id.Description);

			description.Text = MainActivity.Instance.Descrizione;
			description.Typeface = Roboto;
			Console.WriteLine ("MainActivity.Instance.ImageUrl:"+MainActivity.Instance.ImageUrl);
			if (MainActivity.Instance.ImageUrl.CompareTo("null")==0) {
				image.Visibility = ViewStates.Gone;
			} else {
				caricaImmagineAsync (MainActivity.Instance.ImageUrl, image);
			}// Create your application here



		}

		async void caricaImmagineAsync(String uri,ImageView immagine_view){
			WebClient webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);

			Console.WriteLine ("Immagine caricata!");

		}

		protected override int getLayoutResource() {
			return Resource.Layout.NotificationActivityLayout;
		}

		public override void OnBackPressed ()
		{
			
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			Console.WriteLine ("ONPAUSE1 "+MainActivity.Instance.Paused);
			MainActivity.Instance.Paused = true;
			Console.WriteLine ("ONPAUSE2 "+MainActivity.Instance.Paused);
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			Console.WriteLine ("OnResume1 "+MainActivity.Instance.Paused);
			MainActivity.Instance.Paused = false;
			Console.WriteLine ("OnResume2 "+MainActivity.Instance.Paused);
		}

	}
}

