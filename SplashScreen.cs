﻿using System;

using Android.App;
using Android.Content.PM;

using Android.OS;

using Android.Content;
using System.Threading;
using System.Threading.Tasks;


namespace RoccoGiocattoli
{
	[Activity(Label = "RoccoGiocattoli", MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash2", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashScreen : Activity
	{
		bool notification;
		string desc,img,_key;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);


			notification = Intent.GetBooleanExtra ("Notification", false);
			desc = Intent.GetStringExtra ("descrizione");
			img = Intent.GetStringExtra ("image");
			_key = Intent.GetStringExtra ("PushKey");

			SetContentView (Resource.Layout.SplashScreen);
			ThreadPool.QueueUserWorkItem(o=> LoadActivity ());
		}
		public void LoadActivity(){
			Thread.Sleep (1000);
			RunOnUiThread (() => {
				var intent = new Intent(this, typeof(MainActivity));

				if(notification){

					intent.PutExtra ("image",img);
					intent.PutExtra ("descrizione",desc);
					intent.PutExtra ("PushKey",_key);
					intent.PutExtra ("Notification",true);

				}

				Console.WriteLine("2");

				StartActivity(intent);
				Finish();
			});
		}
	}
}

