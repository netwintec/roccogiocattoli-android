﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Facebook;

using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Android.Support.V4.App;
using Android.Content.PM;
using Java.Security;
using Xamarin.Facebook.AppEvents;
using Android.Graphics;
using Android.Views.Animations;

namespace RoccoGiocattoli
{
	[Activity (Label = "@string/app_name",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class LoginPageActivity : Activity
	{
		Button Login;
		ICallbackManager callbackManager;
		LoginButton loginButton;
		EditText Email,Password;
		UtilityLoginManager loginManager=new UtilityLoginManager();
		ImageView imageRotante;
		RelativeLayout Loading;
		Animation rotateAboutCenterAnimation;
	

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			//Facebook Login

			callbackManager = CallbackManagerFactory.Create ();

			var loginCallback = new FacebookCallback<LoginResult> {

				HandleSuccess = loginResult => {
					//login
					Console.WriteLine ("Loggato con successo");
					Console.WriteLine(AccessToken.CurrentAccessToken.Token);

					RunOnUiThread (() => {
						Loading.Visibility = ViewStates.Visible;
						Loading.Invalidate();
					} );

					List<string> result = loginManager.LoginFB(AccessToken.CurrentAccessToken.Token);

					if(result[0] =="Errore"){
						
						RunOnUiThread (() => {
							Loading.Visibility = ViewStates.Invisible;
							Loading.Invalidate();
						} );
						LoginManager.Instance.LogOut();
						AlertDialog.Builder alert = new AlertDialog.Builder (this);
						alert.SetTitle("Errore di rete");
						alert.SetMessage ("Login non Riuscito riprovare più tardi");
						alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
						alert.Show();
					}

					else{
						
						Console.WriteLine(result [0]+"  "+result[1]+"  "+result [2]+"  "+result[3]+"  "+result [4]);
						if(result[4]==""){
							var prefs = MainActivity.Instance.prefs;
							var prefEditor = prefs.Edit();
							prefEditor.PutString("AppoggioTokenN4U",result[1]);
							prefEditor.PutString("NomeN4U",result[2]);
							prefEditor.PutString("CognomeN4U",result[3]);
							prefEditor.Commit();
							var intent = new Intent (this, typeof(CardActivity));
							StartActivity (intent);
							Finish();
						}else {
							var prefs = MainActivity.Instance.prefs;
							var prefEditor = prefs.Edit();
							prefEditor.PutString("TokenN4U",result[1]);
							prefEditor.PutString("NomeN4U",result[2]);
							prefEditor.PutString("CognomeN4U",result[3]);
							prefEditor.Commit();
							var intent = new Intent (this, typeof(HomePageActivity));
							StartActivity (intent);
							Finish();
						}
					}

				},
				HandleCancel = () => {
					Console.WriteLine ("Condizioni non accettate");
				},
				HandleError = loginError => {
					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Errore Login con Facebook");
					alert.SetMessage ("Login non Riuscito riprovare più tardi");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
			};

			LoginManager.Instance.RegisterCallback (callbackManager, loginCallback);

			//Regulag Login

			SetContentView (Resource.Layout.LoginPage);
			ActionBar.Hide ();

			imageRotante = FindViewById<ImageView> (Resource.Id.logoRotante);
			Loading = FindViewById<RelativeLayout> (Resource.Id.loading);

			rotateAboutCenterAnimation = AnimationUtils.LoadAnimation(this,Resource.Animation.RotationAnim);
			imageRotante.StartAnimation(rotateAboutCenterAnimation);

			Loading.Visibility = ViewStates.Invisible;

			loginButton = (LoginButton) FindViewById<Button> (Resource.Id.LoginFb);

			loginButton.SetReadPermissions("public_profile","email");

			Typeface Roboto = Typeface.CreateFromAsset(Assets,"fonts/Roboto_Regular.ttf"); 

			Login = FindViewById<Button> (Resource.Id.LoginButton);
			Login.Typeface = Roboto;
			Login.Click += delegate {

				//var intent = new Intent (this, typeof(HomePageActivity));
				//StartActivity (intent);
				//Finish();
				//
				//imageRotante.StartAnimation(rotateAboutCenterAnimation);

				AppearLoading();
				

				//imageRotante.RefreshDrawableState();

				List<string> result = loginManager.Login(Email.Text,Password.Text);
				Console.WriteLine(result [0]);
					
				if(result[0] == "SUCCESS"){
					Console.WriteLine(result [0]+"  "+result[1]+"  "+result [2]+"  "+result[3]+"  "+result [4]);
					if(result[4]==""){
						var prefs = MainActivity.Instance.prefs;
						var prefEditor = prefs.Edit();
						prefEditor.PutString("AppoggioTokenN4U",result[1]);
						prefEditor.PutString("NomeN4U",result[2]);
						prefEditor.PutString("CognomeN4U",result[3]);
						prefEditor.Commit();
						var intent = new Intent (this, typeof(CardActivity));
						StartActivity (intent);
						Finish();
					}else {
						var prefs = MainActivity.Instance.prefs;
						var prefEditor = prefs.Edit();
						prefEditor.PutString("TokenN4U",result[1]);
						prefEditor.PutString("NomeN4U",result[2]);
						prefEditor.PutString("CognomeN4U",result[3]);
						prefEditor.Commit();
						var intent = new Intent (this, typeof(HomePageActivity));
						StartActivity (intent);
						Finish();
					}
				}

				if(result[0] == "ERROR"){

					RunOnUiThread (() => {
						Loading.Visibility = ViewStates.Invisible;
						Loading.Invalidate();
					} );

					Email.Text="";
					Password.Text="";
					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Login Non Riuscito");
					alert.SetMessage ("E-Mail o Password Errata");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();

				}

				if(result[0] == "ERRORDATA"){

					RunOnUiThread (() => {
						Loading.Visibility = ViewStates.Invisible;
						Loading.Invalidate();
					} );

					Email.Text="";
					Password.Text="";
					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Errore Dati");
					alert.SetMessage ("E-Mail o Password Non Inseriti o Formato errato");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();

				}

						
			};

			TextView PasswordLost = FindViewById<TextView> (Resource.Id.PasswordLostButton);
			PasswordLost.Typeface = Roboto;
			PasswordLost.Click += delegate {

				var uri = Android.Net.Uri.Parse ("http://www.roccogiocattoli.eu/profilo/");
				var intent = new Intent (Intent.ActionView, uri);
				StartActivity (intent);

			};
			FindViewById<TextView> (Resource.Id.textView1).Typeface = Roboto;
			FindViewById<TextView> (Resource.Id.textView2).Typeface = Roboto;
			FindViewById<TextView> (Resource.Id.textView3).Typeface = Roboto;
			Email = FindViewById<EditText> (Resource.Id.EmailText);
			Email.Typeface = Roboto;

			Password = FindViewById<EditText> (Resource.Id.PasswordText);
			Password.Typeface = Roboto;


		}

		public async void AppearLoading(){
			Console.WriteLine("Prova");
			Loading.Visibility = ViewStates.Visible;
			//FindViewById<ScrollView> (Resource.Id.scrollView1).Visibility = ViewStates.Visible;
			//FindViewById<ScrollView> (Resource.Id.scrollView1).Invalidate();
			Loading.Invalidate ();

		}

		public override void OnBackPressed ()
		{
			MainActivity.Instance.CountBluetooth = -1;
			base.OnBackPressed ();
		}
			

		protected override void OnSaveInstanceState (Bundle outState)
		{
			base.OnSaveInstanceState (outState);
		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);

			callbackManager.OnActivityResult (requestCode, (int)resultCode, data);
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			AppEventsLogger.DeactivateApp (this);

			Console.WriteLine ("ONPAUSE1 "+MainActivity.Instance.Paused);
			MainActivity.Instance.Paused = true;
			Console.WriteLine ("ONPAUSE2 "+MainActivity.Instance.Paused);
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			AppEventsLogger.ActivateApp (this);

			Console.WriteLine ("ONRESUME1 "+MainActivity.Instance.Paused);
			MainActivity.Instance.Paused = false;
			Console.WriteLine ("ONRESUME2 "+MainActivity.Instance.Paused);
		}

	}

	class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
	{
		public Action HandleCancel { get; set; }
		public Action<FacebookException> HandleError { get; set; }
		public Action<TResult> HandleSuccess { get; set; }

		public void OnCancel ()
		{
			var c = HandleCancel;
			if (c != null)
				c ();
		}

		public void OnError (FacebookException error)
		{
			var c = HandleError;
			if (c != null)
				c (error);
		}

		public void OnSuccess (Java.Lang.Object result)
		{
			var c = HandleSuccess;
			if (c != null)
				c (result.JavaCast<TResult> ());
		}


	}
	class CustomProfileTracker : ProfileTracker
	{
		public delegate void CurrentProfileChangedDelegate (Profile oldProfile, Profile currentProfile);

		public CurrentProfileChangedDelegate HandleCurrentProfileChanged { get; set; }

		protected override void OnCurrentProfileChanged (Profile oldProfile, Profile currentProfile)
		{
			var p = HandleCurrentProfileChanged;
			if (p != null)
				p (oldProfile, currentProfile);
		}
	}
}

