﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace RoccoGiocattoli
{		
	public class AcquistiObject 
	{
		public string Nome, Data, Prezzo, PV;

		public AcquistiObject(string nome,string data,string prezzo,string pv){

			Nome = nome;
			Data = data;
			Prezzo = prezzo;
			PV = pv;

		}

	}
}

