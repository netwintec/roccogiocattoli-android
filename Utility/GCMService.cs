﻿using System.Text;
using System;
using Android.App;
using Android.Content;
using Android.Util;
using Gcm.Client;


[assembly: UsesPermission (Android.Manifest.Permission.ReceiveBootCompleted)]


namespace RoccoGiocattoli
{
	//You must subclass this!
	[BroadcastReceiver(Permission=Constants.PERMISSION_GCM_INTENTS)]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new string[] { "@PACKAGE_NAME@" })]
	[IntentFilter(new string[] { Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new string[] { "@PACKAGE_NAME@" })]

	public class GcmBroadcastReceiver : GcmBroadcastReceiverBase<PushHandlerService>
	{
		//IMPORTANT: Change this to your own Sender ID!
		//The SENDER_ID is your Google API Console App Project ID.
		//  Be sure to get the right Project ID from your Google APIs Console.  It's not the named project ID that appears in the Overview,
		//  but instead the numeric project id in the url: eg: https://code.google.com/apis/console/?pli=1#project:785671162406:overview
		//  where 785671162406 is the project id, which is the SENDER_ID to use!
		public static string[] SENDER_IDS = new string[] {"672120014777"};

		public const string TAG = "PushSharp-GCM";
	}

	[Service] //Must use the service tag
	public class PushHandlerService : GcmServiceBase
	{
		public PushHandlerService() : base(GcmBroadcastReceiver.SENDER_IDS) { }

		const string TAG = "GCM-SAMPLE";

		protected override void OnRegistered (Context context, string registrationId)
		{
			Log.Verbose(TAG, "GCM Registered: " + registrationId);
			//Eg: Send back to the server
			//	var result = wc.UploadString("http://your.server.com/api/register/", "POST", 
			//		"{ 'registrationId' : '" + registrationId + "' }");

			MainActivity.Instance.PushToken = registrationId;

			//createNotification("GCM Registered...", "The device has been Registered, Tap to View!");
		}

		protected override void OnUnRegistered (Context context, string registrationId)
		{
			Log.Verbose(TAG, "GCM Unregistered: " + registrationId);
			//Remove from the web service
			//	var wc = new WebClient();
			//	var result = wc.UploadString("http://your.server.com/api/unregister/", "POST",
			//		"{ 'registrationId' : '" + lastRegistrationId + "' }");

			//createNotification("GCM Unregistered...", "The device has been unregistered, Tap to View!");
		}

		protected override void OnMessage (Context context, Intent intent)
		{
			Log.Info(TAG, "GCM Message Received!");

			var msg = new StringBuilder();  

			if (intent != null && intent.Extras != null)
			{
				foreach (var key in intent.Extras.KeySet())
					msg.AppendLine(key + "=" + intent.Extras.Get(key).ToString());
			}
				
			Console.WriteLine ("message"+msg);


			string urlimg2 = intent.Extras.Get("image_url").ToString();
			string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/roccogiocattoli/";
			string key2 = "";
			if (urlimg2.Contains ("image")) {
				key2 = urlimg2.Remove (0, 7);
				Console.WriteLine ("IMAGE_URL:"+baseurl + key2);
			} else {
				key2 = "null";
				baseurl ="";
				Console.WriteLine ("IMAGE_URL:"+baseurl + key2);
			}

			bool close2;
			try{
				
				close2 = MainActivity.Instance.IsFinishing;
				MainActivity.Instance.Descrizione = intent.Extras.Get ("description").ToString ();
				MainActivity.Instance.ImageUrl = (baseurl + key2);
				MainActivity.Instance.PushKey =intent.Extras.Get ("_key").ToString ();

			}catch(Exception e){
				close2 = true;
			}

			createNotification("GCM Sample",intent.Extras.Get("title").ToString(),close2,(baseurl + key2),intent.Extras.Get ("description").ToString (),intent.Extras.Get ("_key").ToString ());
		}

		protected override bool OnRecoverableError (Context context, string errorId)
		{
			Log.Warn(TAG, "Recoverable Error: " + errorId);
			return base.OnRecoverableError (context, errorId);
		}

		protected override void OnError (Context context, string errorId)
		{
			Log.Error(TAG, "GCM Error: " + errorId);
		}

		void createNotification(string title, string tit,bool close2,string img,string descr,string _key)
		{


			Console.WriteLine (img+"|"+descr);

			var manager =
				(NotificationManager)this.ApplicationContext.GetSystemService(Context.NotificationService);

			Intent intent;

			if (close2) {

				var intent2 = ApplicationContext.PackageManager.GetLaunchIntentForPackage(this.ApplicationContext.PackageName);
				intent = new Intent (this, typeof(SplashScreen));
				intent.PutExtra ("image",img);
				intent.PutExtra ("descrizione",descr);
				intent.PutExtra ("Notification",true);
				intent.PutExtra ("PushKey", _key);

			} else {
				
				var intent2 = ApplicationContext.PackageManager.GetLaunchIntentForPackage(this.ApplicationContext.PackageName);
				intent = new Intent (this, typeof(NotificationActivity));
				intent.PutExtra ("Notification",true);
				//intent.AddFlags(ActivityFlags.ReorderToFront);

			}

			var pendingIntent = PendingIntent.GetActivity(this.ApplicationContext, 0, intent, PendingIntentFlags.UpdateCurrent);

			var builder = new Notification.Builder(this.ApplicationContext)
				.SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
				.SetSmallIcon(Resource.Drawable.push_icon)
				.SetContentTitle("RoccoGiocattoli")
				.SetContentText (tit)
				.SetContentIntent(pendingIntent)
				.SetAutoCancel(true);

			manager.Notify(1, builder.Build());

		}
	}
}
	