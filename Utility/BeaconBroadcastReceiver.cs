﻿using System;
using System.Json;
using System.Linq;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Support.V4.App;
using RadiusNetworks.IBeaconAndroid;
using System.Json;
using RestSharp;
using Newtonsoft.Json.Linq;
using Android.Util;

[assembly: UsesPermission (Android.Manifest.Permission.ReceiveBootCompleted)]

namespace RoccoGiocattoli
{
	[BroadcastReceiver]
	[IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
	//[IntentFilter(new[] { "com.netwintec.Roccogiocattoli.Beacon" })]

	public class BeaconBroadcastReceiver : BroadcastReceiver,IBeaconConsumer
	{
		private const string UUID = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";
		private const string beaconId = "BlueBeacon";


		int porta =81;
		IBeaconManager _iBeaconManager;
		MonitorNotifier _monitorNotifier;
		RangeNotifier _rangeNotifier;
		RadiusNetworks.IBeaconAndroid.Region _monitoringRegion;
		RadiusNetworks.IBeaconAndroid.Region _rangingRegion;
		Dictionary <string,bool> flagBeacon = new Dictionary <string,bool> ();
		public bool flagTokenN4U = true;
		public Dictionary<string,Beacon> ListBeacon = new Dictionary<string,Beacon> ();
		public List<Messaggio> ListMessaggi = new List <Messaggio> ();
		Button Login,Registrati;
		public Dictionary<int,XmlObject> XmlObjectDictionary = new Dictionary<int,XmlObject>();
		public int totalValue;
		public ISharedPreferences prefs;
		int i=0;

		string Descrizione,ImageUrl;

		public override void OnReceive (Context context, Intent intent)
		{
			Toast.MakeText (context, "Beacon start!", ToastLength.Short).Show ();

			prefs = Application.Context.GetSharedPreferences ("RoccoGiocattoli", FileCreationMode.Private);

			_iBeaconManager = IBeaconManager.GetInstanceForApplication(Application.Context);

			_monitorNotifier = new MonitorNotifier();
			_rangeNotifier = new RangeNotifier();

			_monitoringRegion = new RadiusNetworks.IBeaconAndroid.Region(beaconId, UUID, null, null);
			_rangingRegion = new RadiusNetworks.IBeaconAndroid.Region(beaconId, UUID, null, null);

			_iBeaconManager.Bind (this);

			_iBeaconManager.SetMonitorNotifier (_monitorNotifier);
			_iBeaconManager.SetRangeNotifier (_rangeNotifier);

			Console.WriteLine ("parto2");

			_rangeNotifier.DidRangeBeaconsInRegionComplete += RangingBeaconsInRegion;
		}


		void RangingBeaconsInRegion(object sender, RangeEventArgs e){
			string TokenN4U = prefs.GetString ("TokenN4U", null);
			bool Scan;
			if (TokenN4U == null && TokenN4U == "") {
				Scan = false;
				flagTokenN4U = true;
			} else 
			{
				Scan = true;
			}
			Console.WriteLine ("Token "+Scan);
			if (flagTokenN4U == true) {
				if (TokenN4U != null  && TokenN4U != "") {
					//
					var client = new RestClient ("http://api.netwintec.com:"+porta+"/");
					var requestMessage = new RestRequest ("active-messages", Method.GET);
					requestMessage.AddHeader ("content-type", "application/json");
					requestMessage.AddHeader ("Net4U-Company", "roccogiocattoli");
					requestMessage.AddHeader ("Net4U-Token", prefs.GetString ("TokenN4U", null));

					IRestResponse response = client.Execute (requestMessage);

					//string prova = "{\"messages\":[{\"title\":\"titolo\",\"description\":\"descrizione\",\"image_url\":\"/image/4fae906a-0e25-4f1d-953c-6d8cf1cc0219\",\"content_url\":\"url\",\"type\":\"ibeacon\",\"timestamp_start\":1429800825,\"timestamp_end\":1450882425,\"distance\":5,\"_type\":\"message\",\"_key\":\"message::e68461b7-13f9-4296-856e-3bf83ffd587a\"}]}";
					Console.WriteLine ("RESPONSE:"+response.Content);
					JsonValue json = JsonValue.Parse (response.Content);

					JsonValue data = json ["messages"];

					foreach (JsonValue dataItem in data) {
						var titolo = dataItem ["title"];
						var urlimg = dataItem ["image_url"];
						var descr = dataItem ["description"];
						var action = dataItem ["content_url"];
						var distanzaAp = dataItem ["threshold"];
						float distanzaFl=0;
						if (distanzaAp.ToString ().Contains (".")) {
							string distStr = distanzaAp.ToString ().Replace ('.', ',');
							distanzaFl = float.Parse (distStr);
						} else {
							distanzaFl = float.Parse (distanzaAp.ToString ());
						}
						Console.WriteLine (distanzaFl);
						int distanza = (int)(distanzaFl*100);
						Console.WriteLine (distanza);
						var key = dataItem ["_key"];

						string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/roccogiocattoli/";
						string urlimg2 = urlimg;
						string key2 = urlimg2.Remove (0,7);
						Console.WriteLine (baseurl + key2);

						Console.WriteLine (titolo + "  " + baseurl + key2 + "   " + descr + "   " + action);

						ListMessaggi.Add (new Messaggio (titolo, baseurl + key2, descr, action, key, false));

						var client2 = new RestClient ("http://api.netwintec.com:"+porta+"/");
						var requestBeacon = new RestRequest ("active-messages/" + key, Method.GET);
						requestBeacon.AddHeader ("content-type", "application/json");
						requestBeacon.AddHeader ("Net4U-Company", "roccogiocattoli");
						requestBeacon.AddHeader ("Net4U-Token", prefs.GetString ("TokenN4U", null));

						IRestResponse response2 = client2.Execute (requestBeacon);

						Console.WriteLine (response2.Content);

						JsonValue json2 = JsonValue.Parse (response2.Content);
						JsonValue data2 = json2 ["beacons"];

						foreach (JsonValue dataItem2 in data2) {
							var uuid2 = dataItem2 ["uuid"];
							var majorAp = dataItem2 ["major"];
							int major = int.Parse (majorAp.ToString ());
							var minorAp = dataItem2 ["minor"];
							int minor = int.Parse (minorAp.ToString ());
							Console.WriteLine (uuid2 + "|" + major + "|" + minor + "|" + distanza);
							string Appoggio;
							Appoggio = major.ToString () + "," + minor.ToString () + "," + i;
							ListBeacon.Add (Appoggio, new Beacon (uuid2, major, minor, distanza, i));
							flagBeacon.Add (Appoggio, false);
							if (flagBeacon.ContainsKey (Appoggio) == true) {
								Console.WriteLine ("key:"+Appoggio);
								Console.WriteLine ("c'è");
							}
						}
						i++;
					}
					flagTokenN4U = false;
					Console.WriteLine ("Fine");
				}
			} else {
				if(Scan){
					if (e.Beacons.Count > 0) {
						var s2 = e.Beacons;
						foreach (IBeacon beacon in s2) {
							
							Console.WriteLine ("   " + beacon.Major.ToString () + "  " + beacon.Minor.ToString () + "  " + beacon.Rssi);
							string App2;
							List<string> BeacAppoggioCount = new List <string> ();



							int countBeac = 0;
							for (int q = 0; q < ListMessaggi.Count; q++) {
								App2 = beacon.Major.ToString () + "," + beacon.Minor.ToString () + "," + q;
								Console.WriteLine ("app2:" + beacon.Major.ToString () + "," + beacon.Minor.ToString () + "," + q + "|" + flagBeacon.ContainsKey (App2));
								if (flagBeacon.ContainsKey (App2) == true) {

									BeacAppoggioCount.Add (App2);
									countBeac++;

								}
							}
							Console.WriteLine (countBeac);
							if (countBeac != 0) {
								int a = 0;
								for (int w = 0; w < countBeac; w++) {
									Beacon BeaconApp;
									BeaconApp = ListBeacon [BeacAppoggioCount [w]];
									Console.WriteLine ("Visto" + BeaconApp.IdMessaggio + ":" + ListMessaggi [BeaconApp.IdMessaggio].Visto);
									if (ListMessaggi [BeaconApp.IdMessaggio].Visto == false) {
										int distRssi = 0;

										if (BeaconApp.Distance <= 100)
											distRssi = (int)(-60);

										if (BeaconApp.Distance > 100 && BeaconApp.Distance <= 500)
											distRssi = (int)(-67);

										if (BeaconApp.Distance > 500 && BeaconApp.Distance <= 1000)
											distRssi = (int)(-79);

										if (BeaconApp.Distance > 1000 && BeaconApp.Distance <= 2000)
											distRssi = (int)(-88);

										if (BeaconApp.Distance > 2000)
											distRssi = (int)(-90);

										if (beacon.Rssi > distRssi) {
											Messaggio MessaggioApp;
											MessaggioApp = ListMessaggi [BeaconApp.IdMessaggio];
											flagBeacon [BeacAppoggioCount [w]] = true;
											ListMessaggi [BeaconApp.IdMessaggio].Visto = true;

											ShowNotification (MessaggioApp.Titolo);

											Descrizione = MessaggioApp.Descrizione;
											ImageUrl = MessaggioApp.UrlImage;

											var intent = new Intent (ApplicationContext, typeof(NotificationActivity));
											intent.PutExtra ("Beacons",true);
											Application.Context.StartActivity (intent);

											Console.WriteLine (MessaggioApp.Titolo + "   " + MessaggioApp.Descrizione);
											w = ListMessaggi.Count + 1;
											a = e.Beacons.Count + 1;
										}
									}
								}
								if (a == e.Beacons.Count + 1)
									break; //esco dal foreach
							}
						}
					}
				}
			}
		}

		#region IBeaconConsumer impl
		public void OnIBeaconServiceConnect()
		{
			Console.WriteLine ("Connesso  Al beacon");

			_iBeaconManager.StartMonitoringBeaconsInRegion(_monitoringRegion);
			_iBeaconManager.StartRangingBeaconsInRegion(_rangingRegion);
		}

		public bool BindService (Intent intent, IServiceConnection serviceConnection, Bind flags)
		{
			return true;
			//throw new NotImplementedException ();
		}

		public void UnbindService (IServiceConnection p0)
		{
			//throw new NotImplementedException ();
		}

		public Context ApplicationContext {
			get {
				return Application.Context;//throw new NotImplementedException ();
			}
		}
		#endregion


		private void ShowNotification(string message)
		{
			var resultIntent = new Intent(ApplicationContext, typeof(NotificationActivity));
			resultIntent.AddFlags(ActivityFlags.ReorderToFront);
			var pendingIntent = PendingIntent.GetActivity(ApplicationContext, 0, resultIntent, PendingIntentFlags.UpdateCurrent);

			NotificationCompat.Builder builder = new NotificationCompat.Builder(ApplicationContext)
				.SetDefaults((int)(NotificationDefaults.Sound | NotificationDefaults.Vibrate))
				.SetSmallIcon(Resource.Drawable.Icon_App)
				.SetContentTitle("RoccoGiocattoli")
				.SetContentText (message)
				.SetContentIntent(pendingIntent)
				.SetAutoCancel(true);

			var notification = builder.Build();


			var notificationManager = (NotificationManager)ApplicationContext.GetSystemService(Context.NotificationService);
			notificationManager.Notify(0, notification);
		}
	}
}

