﻿using System;
using System.Collections.Generic;
using System.Json;
using System.IO;


namespace RoccoGiocattoli
{		
	public class JsonUtility
	{
		public JsonUtility(){

		}

		public void SpacchettamentoJsonSaldo(List<string> ListDatiTessera,string content){

			//StreamReader strm = new StreamReader (Assets.Open ("Marker.json"));

			JsonValue json = JsonValue.Parse (content);
			//Console.WriteLine (response.Content);
			JsonValue data1 = json ["saldo"];


			JsonValue data = data1 [0];

			foreach (JsonValue dataItem in data) {

				ListDatiTessera.Add(dataItem["SALDOPUNTI"].ToString());
				ListDatiTessera.Add(dataItem["EANTESSERA"]);

				//Console.WriteLine ("1"+dataItem["SALDOPUNTI"]+"  "+dataItem["EANTESSERA"]);
				//Console.WriteLine ("2"+saldo +"  "+ean);

			}
		}

		public void SpacchettamentoJsonMovimenti(List<AcquistiObject> ListAcquistiObject,string content){

			//StreamReader strm = new StreamReader (Assets.Open ("Marker.json"));

			JsonValue json = JsonValue.Parse (content);
			//Console.WriteLine (response.Content);
			JsonValue data1 = json ["movimenti"];


			JsonValue data = data1 [0];

			int x = 0;
			string nome,dataAcq,prezzo,pv;
			foreach (JsonValue dataItem in data) {

				nome=dataItem["DescrizioneArticolo"];
				if(nome ==null)
					nome = "Non Disp";
				prezzo=dataItem["Valore"].ToString();
				pv=dataItem["DES_PV"];
				string dataNotForm=dataItem["DataTra"];

				dataAcq = dataNotForm.Substring(8,2)+"/"+dataNotForm.Substring(5,2)+"/"+dataNotForm.Substring(0,4);

				Console.WriteLine (nome +" | "+prezzo+"€ | "+pv+" | "+dataAcq+" | "+dataNotForm);

				ListAcquistiObject.Add (new AcquistiObject(nome,dataAcq,prezzo,pv));

				x++;
				if (x == 10)
					break;
			}
		}

		public void SpacchettamentoJsonPremi(List<int> ListPunti,List<int> ListBuono,string content){

			//StreamReader strm = new StreamReader (Assets.Open ("Marker.json"));

			JsonValue json = JsonValue.Parse (content);
			//Console.WriteLine (response.Content);
			JsonValue data1 = json ["premi"];


			JsonValue data = data1 [0];

			foreach (JsonValue dataItem in data) {

				ListPunti.Add (dataItem["PUNTI"]);
				ListBuono.Add (dataItem["Valore"]);


			}
		}
	}
}




