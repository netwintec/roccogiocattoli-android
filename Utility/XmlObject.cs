﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace RoccoGiocattoli
{		
	public class XmlObject 
	{
		public string Nome,Url,Image;
		public Bitmap imagetBit;

		public XmlObject(string nome,string url,string image){
		
			Nome = nome;
			Url = url;
			Image = image;
		
		}

	}
}

