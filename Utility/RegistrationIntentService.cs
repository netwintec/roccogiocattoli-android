﻿using System;
using Android.App;
using Android.Content;
using Android.Util;
using Android.Gms.Gcm;
using Android.Gms.Gcm.Iid;
using Newtonsoft.Json.Linq;
using RestSharp;


namespace RoccoGiocattoli
{
	[Service(Exported = false)]
	class RegistrationIntentService : IntentService
	{
		static object locker = new object();

		public RegistrationIntentService() : base("RegistrationIntentService") { }

		protected override void OnHandleIntent (Intent intent)
		{
			try
			{
				Log.Info ("RegistrationIntentService", "Calling InstanceID.GetToken");
				lock (locker)
				{
					var instanceID = InstanceID.GetInstance (this);
					var token = instanceID.GetToken (
						"672120014777", GoogleCloudMessaging.InstanceIdScope, null);

					Log.Info ("RegistrationIntentService", "GCM Registration Token: " + token);
					SendRegistrationToAppServer (token);
					Subscribe (token);
					Console.WriteLine("TOKEN GCM:"+token);


				}
			}
			catch (Exception e)
			{
				Log.Debug("RegistrationIntentService", "Failed to get a registration token");
				return;
			}
		}

		void SendRegistrationToAppServer (string token)
		{
			MainActivity.Instance.PushToken = token;
		}

		void Subscribe (string token)
		{
			Console.WriteLine ("Registration");
			var pubSub = GcmPubSub.GetInstance(this);
			pubSub.Subscribe(token, "/topics/global", null);
			

		}
			
	}
}

