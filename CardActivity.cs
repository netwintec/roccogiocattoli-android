﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Facebook;

using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Android.Support.V4.App;
using Android.Content.PM;
using Java.Security;
using Xamarin.Facebook.AppEvents;
using Android.Graphics;
using Android.Telephony;
using ZXing;
using ZXing.Mobile;
using RestSharp;
using Newtonsoft.Json.Linq;

namespace RoccoGiocattoli
{
	[Activity (Label = "CardActivity",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class CardActivity : Activity
	{
		MobileBarcodeScanner scanner;
		int porta = 81;
		ISharedPreferences prefs;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			scanner = new MobileBarcodeScanner();

			TelephonyManager manager = (TelephonyManager)Application.Context.GetSystemService(Context.TelephonyService);
			if(manager.PhoneType == PhoneType.None){
				SetContentView (Resource.Layout.CardPage_Tablet);
			}else{
				SetContentView (Resource.Layout.CardPage);
			}
			ActionBar.Hide ();

			Typeface Bubble = Typeface.CreateFromAsset(Assets,"fonts/Bubblegum.ttf");
			Typeface Roboto = Typeface.CreateFromAsset(Assets,"fonts/Roboto_Regular.ttf");

			FindViewById<TextView> (Resource.Id.textView1).Text="Possiedi già\nla carta fedeltà?";
			FindViewById<TextView> (Resource.Id.textView2).Text="Non possiedi\nla carta fedeltà?";

			FindViewById<TextView> (Resource.Id.textView1).Typeface=Bubble;
			FindViewById<TextView> (Resource.Id.textView2).Typeface=Bubble;

			Button setCard = FindViewById<Button> (Resource.Id.button1);
			setCard.Typeface = Roboto;
			setCard.Click += async delegate {

				//Tell our scanner to use the default overlay
				scanner.UseCustomOverlay = false;

				//We can customize the top and bottom text of the default overlay
				scanner.TopText = "Tieni la camera davanti al barcode\na circa 6 pollici";
				scanner.BottomText = "Attendi che il barcode venga scansionato!";

				//Start scanning
				var result = await scanner.Scan();

				HandleScanResult(result);
			};

			Button rndCard = FindViewById<Button> (Resource.Id.button2);
			rndCard.Typeface = Roboto;
			rndCard.Click += delegate {

				prefs = MainActivity.Instance.prefs;

				
				var client2 = new RestClient("http://api.netwintec.com:" + porta + "/");
				//client.Authenticator = new HttpBasicAuthenticator(username, password);

				var requestN4U2 = new RestRequest("card", Method.GET);
				requestN4U2.AddHeader("content-type", "application/json");
				requestN4U2.AddHeader("Net4U-Company", "roccogiocattoli");
				requestN4U2.AddHeader("Net4U-Token", prefs.GetString ("AppoggioTokenN4U", null));

				IRestResponse response2 = client2.Execute(requestN4U2);

				Console.WriteLine(response2.StatusCode + "   " + response2.Content);

				if (response2.StatusCode == System.Net.HttpStatusCode.OK)
				{
					var prefEditor = prefs.Edit();
					prefEditor.PutString("TokenN4U", prefs.GetString ("AppoggioTokenN4U", null));

					prefEditor.Commit();
					var intent = new Intent (this, typeof(HomePageActivity));
					StartActivity (intent);
					Finish();
				}

				else
				{
					Errore();
				}
			};

			// Create your application here
		}

		void HandleScanResult (ZXing.Result result)
		{


			string msg = "";

			if (result != null && !string.IsNullOrEmpty(result.Text))
				SetCard(result.Text);
			else
			{
				msg = "Scansione Interrotta";
				this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
			}


		}

		public void SetCard (string code){

			Console.WriteLine("scan"+code);

			prefs = MainActivity.Instance.prefs;


			var client2 = new RestClient("http://api.netwintec.com:" + porta + "/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			var requestN4U2 = new RestRequest("card", Method.POST);
			requestN4U2.AddHeader("content-type", "application/json");
			requestN4U2.AddHeader("Net4U-Company", "roccogiocattoli");
			requestN4U2.AddHeader("Net4U-Token", prefs.GetString ("AppoggioTokenN4U", null));

			JObject oJsonObject = new JObject();

			oJsonObject.Add("tessera", code);

			requestN4U2.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

			IRestResponse response2 = client2.Execute(requestN4U2);

			Console.WriteLine(response2.StatusCode + "   " + response2.Content);

			if (response2.StatusCode == System.Net.HttpStatusCode.OK)
			{
				var prefEditor = prefs.Edit();
				prefEditor.PutString("TokenN4U", prefs.GetString ("AppoggioTokenN4U", null));

				prefEditor.Commit();
				var intent = new Intent (this, typeof(HomePageActivity));
				StartActivity (intent);
				Finish();
			}

			else
			{
				Errore();
			}


		}

		public async void Errore()
		{
			AlertDialog.Builder alert = new AlertDialog.Builder (this);
			alert.SetTitle ("Errore di rete\nAssegnazione Card non riuscita.\nTornare al Menù?");
			alert.SetPositiveButton ("OK", (senderAlert, args) => {
				//chiamata per sloggarsi
				var prefEditor = prefs.Edit();
				prefEditor.PutString("TokenN4U","");
				prefEditor.Commit();
				LoginManager.Instance.LogOut();
				Finish ();
			} );
			alert.SetNegativeButton ("Riprova", (senderAlert, args) => {
				//volendo fa qualcosa
			} );
			//fa partire l'alert su di un trhead
			RunOnUiThread (() => {
				alert.Show();
			} );
		}
	}
}

