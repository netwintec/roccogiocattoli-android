﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content.PM;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Views;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Newtonsoft.Json.Linq;
using RestSharp;
using Android.Bluetooth;

namespace RoccoGiocattoli
{
	[Activity (Label = "HomePage",Theme = "@style/AppTheme",ScreenOrientation = ScreenOrientation.Portrait)]			
	public class HomePageActivity : BaseActivity 
	{

		private DrawerLayout mDrawerLayout;
		private ListView mDrawerList;

		//List<DrawerItem> dataList;
		DrawerAdapter adapter;

		public ImageView menuIcon;
		public ImageView backIcon;

		public FragmentManager.IOnBackStackChangedListener backStackListener;
		ISharedPreferences prefs;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			setActionBarIcon(Resource.Drawable.menu_toolbar,Resource.Drawable.back);

			prefs = MainActivity.Instance.prefs;

			if (MainActivity.Instance.CountBluetooth == 2) {
				AlertDialog.Builder builder = new AlertDialog.Builder (this);
				builder.SetTitle ("Incredibili Promozioni");
				builder.SetMessage ("Accendi il Bluetooth e cerca le incredibili promozioni in negozio.");
				builder.SetCancelable (false);
				builder.SetPositiveButton ("OK", delegate {
				});

				BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;

				if (!bluetoothAdapter.IsEnabled && !MainActivity.Instance.BluetoothAlert) {
					builder.Show ();
					MainActivity.Instance.BluetoothAlert = true;
				}
			}

			if (MainActivity.Instance.Notification) {

				var intent = new Intent (this, typeof(NotificationActivity));
				intent.PutExtra ("Notification",true);
				this.StartActivity (intent);

			}

			MainActivity.Instance.TokenN4U = prefs.GetString ("TokenN4U", null);
			MainActivity.Instance.Scan = true;

			//Set default settings
			//PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

			// Set Navigation Drawer
			//dataList = new ArrayList<>();
			mDrawerLayout =  FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			mDrawerList =  FindViewById<ListView>(Resource.Id.right_drawer);
			mDrawerList.SetFooterDividersEnabled (false);
			mDrawerList.SetHeaderDividersEnabled (false);
			mDrawerList.Divider.SetAlpha (0);

			//mDrawerList.Adapter = new ArrayAdapter<string> (this, Resource.Layout.item_menu, Section);
			adapter = new DrawerAdapter(this);

			mDrawerList.SetAdapter(adapter);

			menuIcon = (ImageView) getToolbar().FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView) getToolbar().FindViewById(Resource.Id.back_icon);
			backIcon.Visibility = ViewStates.Invisible;

			menuIcon.Click += delegate {
				if (mDrawerLayout.IsDrawerOpen(GravityCompat.End)) {
					mDrawerLayout.CloseDrawer(GravityCompat.End);
				//	menuIcon.SetImageResource(Resource.Drawable.menu_toolbar);
				}else{
					mDrawerLayout.OpenDrawer(GravityCompat.End);
				//	menuIcon.SetImageResource(Resource.Drawable.menu_toolbar_att);
				}
			};
			//mDrawerLayout.SetDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

			backIcon.Click += delegate {
				Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
				if(SupportFragmentManager.BackStackEntryCount == 0) {

				} else {
					//int i=SupportFragmentManager.BackStackEntryCount;
					//while(i>0)
					SupportFragmentManager.PopBackStack();
					//if(FragmentBeaconImage.player !=null){
					//	FragmentBeaconImage.player.Release();
					//}
				}
			};

			mDrawerList.ItemClick+=DrawerListItemClick;
			mDrawerLayout.DrawerClosed+= delegate {
				menuIcon.SetImageResource (Resource.Drawable.menu_toolbar);
			};
			mDrawerLayout.DrawerOpened += delegate {
				menuIcon.SetImageResource (Resource.Drawable.menu_toolbar_att);
			};
			//Initialize fragment
			Fragment fragment;
			//Bundle args = new Bundle();
			fragment = new FragmentHome();
			//args.putString(FragmentHome.ITEM_NAME, dataList.get(1).getItemName());
			//fragment.setArguments(args);
			FragmentManager frgManager = SupportFragmentManager;
			frgManager.BeginTransaction().Replace(Resource.Id.content_frame, fragment)
				.Commit();

			SupportFragmentManager.BackStackChanged += delegate {
				Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
				if(SupportFragmentManager.BackStackEntryCount == 0) {
					backIcon.Visibility = ViewStates.Invisible;
				} else {
					backIcon.Visibility = ViewStates.Visible;
					//backIcon.SetImageResource(Resource.Drawable.back_icon);
				}
			};



			//********** SEND NOTIFICATION TOKEN ******************
			var client = new RestClient("http://api.netwintec.com:81/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			var requestN4U = new RestRequest("notification", Method.POST);
			requestN4U.AddHeader("content-type", "application/json");
			requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
			requestN4U.AddHeader ("Net4U-Token",prefs.GetString ("TokenN4U", null));
			requestN4U.Timeout = 60000;

			JObject oJsonObject = new JObject();

			oJsonObject.Add("uuid", MainActivity.Instance.PushToken);
			oJsonObject.Add("type", "android");

			requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

			Console.WriteLine ("notification/1:\""+MainActivity.Instance.PushToken+"\"");
			if(MainActivity.Instance.PushToken !=null)
				client.ExecuteAsync(requestN4U,null);
			Console.WriteLine ("notification/2");
			//Console.WriteLine("Result:" + response.Content);

			//************************************************


		}

		protected override int getLayoutResource() {
			return Resource.Layout.HomePageLayout;
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			//case Resource.Id.home:
			//	mDrawerLayout.OpenDrawer(GravityCompat.Start);
			//	return true;
			}

			return base.OnOptionsItemSelected (item);
		}

		public override void OnBackPressed ()
		{
			Console.WriteLine("INFO"+ (SupportFragmentManager.BackStackEntryCount).ToString());
			if (mDrawerLayout.IsDrawerOpen(GravityCompat.End)) {
				mDrawerLayout.CloseDrawer(GravityCompat.End);
			} else {
				if (SupportFragmentManager.BackStackEntryCount == 0) {
					LogOut ();
				}
			}
		}

		public void LogOut(){
			//Alert vuoi sloggarti?

			MainActivity.Instance.CountBluetooth = -1;

			AlertDialog.Builder alert = new AlertDialog.Builder (this);
			alert.SetTitle ("Sicuro di voler effettuare il logout?");
			alert.SetPositiveButton ("Logout", (senderAlert, args) => {


				var prefs = MainActivity.Instance.prefs;

				//********** DELETE NOTIFICATION TOKEN ******************
				var client = new RestClient("http://api.netwintec.com:81/");
				//client.Authenticator = new HttpBasicAuthenticator(username, password);

				var requestN4U = new RestRequest("notification", Method.DELETE);
				requestN4U.AddHeader("content-type", "application/json");
				requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
				requestN4U.AddHeader ("Net4U-Token",prefs.GetString ("TokenN4U", null));
				requestN4U.Timeout = 60000;

				JObject oJsonObject = new JObject();

				oJsonObject.Add("uuid", MainActivity.Instance.PushToken);
				oJsonObject.Add("type", "android");

				requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

				Console.WriteLine ("1");
				//client.ExecuteAsync(requestN4U,null);
				if(MainActivity.Instance.PushToken !=null)
					client.ExecuteAsync(requestN4U, (s,e) => {

						Console.WriteLine("Logout"+s.Content+"!"+s.StatusCode);

					});
				
				Console.WriteLine ("2");
				//Console.WriteLine("Result:" + response.Content);

				//************************************************

				//chiamata per sloggarsi
				MainActivity.Instance.TokenN4U = "";
				MainActivity.Instance.flagTokenN4U = true;
				MainActivity.Instance.Scan = false;


				var prefEditor = prefs.Edit();
				prefEditor.PutString("TokenN4U","");
				prefEditor.Commit();
				LoginManager.Instance.LogOut();
				Finish ();

			} );
			alert.SetNegativeButton ("Annulla", (senderAlert, args) => {
				//volendo fa qualcosa
			} );
			//fa partire l'alert su di un trhead
			RunOnUiThread (() => {
				alert.Show();
			} );
		}

		public void DrawerListItemClick ( object sender , AdapterView.ItemClickEventArgs item ){
			selectItem(item.Position);
		}

		public void selectItem(int position) {
			Fragment fragment = null;
			Bundle args = new Bundle();
			bool isFragment = false;

			switch (position) {
			case 1:
				if (SupportFragmentManager.BackStackEntryCount == 1) {
					SupportFragmentManager.PopBackStack ();
				}
				if (SupportFragmentManager.BackStackEntryCount ==2 ) {
					SupportFragmentManager.PopBackStackImmediate();
					SupportFragmentManager.PopBackStackImmediate();
				}
				break;
			case 2:
				fragment = new FragmentRocco();
				isFragment = true;
				break;
			case 3:
				fragment = new FragmentProfilo();
				isFragment = true;
				break;
			case 4:
				fragment = new FragmentNews();
				isFragment = true;
				break;
			case 5:
				fragment = new FragmentPuntiVendita();
				isFragment = true;
				break;
			case 6:
				var email = new Intent (Android.Content.Intent.ActionSend);
				email.PutExtra (Android.Content.Intent.ExtraEmail,
					new string[]{"info@roccogiocattoli.com"} );
				//email.PutExtra (Android.Content.Intent.ExtraSubject, "Hello Email");
				//email.PutExtra (Android.Content.Intent.ExtraText,
				//	"Hello from Xamarin.Android");
				email.SetType ("message/rfc822");
				StartActivity (email);
				//var intent = new Intent(this, typeof(CosaVedereActivity));
				//StartActivity(intent);
				break;
			case 7:
				LogOut ();
				break;

			default:
				break;
			}


			if(isFragment) {
				int i = SupportFragmentManager.BackStackEntryCount;
				while(i>0)
				{
					SupportFragmentManager.PopBackStackImmediate();
					i = SupportFragmentManager.BackStackEntryCount;
				}

				FragmentManager frgManager = SupportFragmentManager;
				frgManager.BeginTransaction()
					.Replace(Resource.Id.content_frame, fragment)
					.AddToBackStack(null)
					.Commit();


			}
			mDrawerList.SetItemChecked(position,false);
			mDrawerLayout.CloseDrawer(mDrawerList);
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			Console.WriteLine ("ONPAUSE1 "+MainActivity.Instance.Paused);
			if(MainActivity.Instance.Paused)
				MainActivity.Instance.Paused = false;
			else
				MainActivity.Instance.Paused = true;
			Console.WriteLine ("ONPAUSE2 "+MainActivity.Instance.Paused);
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			Console.WriteLine ("ONRESUME1 "+MainActivity.Instance.Paused);
			if(MainActivity.Instance.Paused)
				MainActivity.Instance.Paused = false;
			else
				MainActivity.Instance.Paused = true;
			Console.WriteLine ("ONRESUME2 "+MainActivity.Instance.Paused);
		}
	}
		
}



