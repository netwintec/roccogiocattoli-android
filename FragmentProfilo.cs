﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Telephony;
using RestSharp;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Android.Views.Animations;
using System.Threading;
using System.Threading.Tasks;

namespace RoccoGiocattoli
{
	public class FragmentProfilo : Fragment
	{
		LinearLayout ContentView;
		View DettagliAcquistiView;
		View ProfiloView,PremiView,AcquistiView;
		float density;
		TextView PointText,NomeText,CognomeText,TesseraText;
		int porta =81;
		JsonUtility ju = new JsonUtility();
		List<int> ListPunti=new List<int> ();
		List<int> ListBuono=new List<int> ();
		List<string> ListDatiTessera=new List<string> ();
		List<AcquistiObject> ListAquistiObject=new List<AcquistiObject> ();
		int saldoPunti;
		string EanTessera;
		bool flagA1=false,flagA2=false,flagA3=false,flagA4=false,flagA5=false,flagA6=false,flagA7=false,flagA8=false,flagA9=false,flagA10=false;
		ISharedPreferences prefs;
		bool notRetrive;
		ImageView image;
		View view;



		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}



		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment

			view = inflater.Inflate(Resource.Layout.FragmentProfilo, container, false);

			view.FindViewById<RelativeLayout> (Resource.Id.relativeLayout1).Visibility = ViewStates.Invisible;
			view.FindViewById<RelativeLayout> (Resource.Id.relativeLayout5).Visibility = ViewStates.Visible;

			image = view.FindViewById<ImageView> (Resource.Id.imageView2);

			var rotateAboutCenterAnimation = AnimationUtils.LoadAnimation(Activity.ApplicationContext,Resource.Animation.RotationAnim);
			image.StartAnimation(rotateAboutCenterAnimation);

			TelephonyManager manager = (TelephonyManager)Application.Context.GetSystemService(Context.TelephonyService);
			if(manager.PhoneType == PhoneType.None){
				ProfiloView = inflater.Inflate(Resource.Layout.ProfiloView_Tablet,container,false);
			}else{
				ProfiloView = inflater.Inflate(Resource.Layout.ProfiloView,container,false);
			}
			//ProfiloView = inflater.Inflate(Resource.Layout.ProfiloView,container,false);

			AcquistiView = inflater.Inflate(Resource.Layout.AcquistiView,container,false);

			DettagliAcquistiView = inflater.Inflate(Resource.Layout.DettagliAcquistiView,container,false);
			//DettagliAcquistiView = Appoggio.FindViewById<RelativeLayout> (Resource.Id.DettagiLayout);

			//TelephonyManager manager2 = (TelephonyManager)Application.Context.GetSystemService(Context.TelephonyService);
			if(manager.PhoneType == PhoneType.None){
				PremiView = inflater.Inflate(Resource.Layout.PremiView_Tablet,container,false);
			}else{
				PremiView = inflater.Inflate(Resource.Layout.PremiView,container,false);
			}

			new Thread(new ThreadStart(() => {
				//Activity.RunOnUiThread (() => {
				//	Task.Delay(3000);
					CaricaDati(); // this works!
			//	});// should NOT reference UILabel on background thread!
			})).Start();
			return view;
		}

		public async void CaricaDati ()
		{


			prefs = MainActivity.Instance.prefs;

			var somePref = prefs.GetString ("TokenN4U", null);
			Console.WriteLine ("Token:"+somePref);

			//********Ritiro dati per profilo********
			var client = new RestClient ("http://api.netwintec.com:"+porta+"/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			var requestN4U = new RestRequest ("saldo", Method.GET);
			requestN4U.AddHeader ("content-type", "application/json");
			requestN4U.AddHeader ("Net4U-Company", "roccogiocattoli");
			requestN4U.AddHeader ("Net4U-Token",prefs.GetString ("TokenN4U", null));  // "e74e8d8c-b4c5-482b-9f0e-d5fe2f657c1b"

			IRestResponse response = client.Execute (requestN4U);

			if (response.StatusCode == System.Net.HttpStatusCode.OK)
			{
				Console.WriteLine(response.Content);
				ju.SpacchettamentoJsonSaldo(ListDatiTessera, response.Content);
			}
			else
			{
				ListDatiTessera.Add("0");
				ListDatiTessera.Add("");
			}

			Console.WriteLine (ListDatiTessera[0] +"  "+ListDatiTessera[1]);
			EanTessera = ListDatiTessera [1];
			saldoPunti = int.Parse(ListDatiTessera [0]);

			//*************************************

			//********Ritiro dati per acquisto********
			var client2 = new RestClient ("http://api.netwintec.com:"+porta+"/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			var requestN4U2 = new RestRequest ("movimenti", Method.GET);
			requestN4U2.AddHeader ("content-type", "application/json");
			requestN4U2.AddHeader ("Net4U-Company", "roccogiocattoli");
			requestN4U2.AddHeader ("Net4U-Token",prefs.GetString ("TokenN4U", null));  // "e74e8d8c-b4c5-482b-9f0e-d5fe2f657c1b"

			IRestResponse response2 = client2.Execute (requestN4U2);

			if (response2.StatusCode == System.Net.HttpStatusCode.OK)
			{
				Console.WriteLine(response2.Content);
				ju.SpacchettamentoJsonMovimenti(ListAquistiObject, response2.Content);
			}


			//*************************************

			//********Ritiro dati per premi********
			var client3 = new RestClient ("http://api.netwintec.com:"+porta+"/");
			//client.Authenticator = new HttpBasicAuthenticator(username, password);

			var requestN4U3 = new RestRequest ("premi", Method.GET);
			requestN4U3.AddHeader ("content-type", "application/json");
			requestN4U3.AddHeader ("Net4U-Company", "roccogiocattoli");
			requestN4U3.AddHeader ("Net4U-Token",prefs.GetString ("TokenN4U", null));  // "e74e8d8c-b4c5-482b-9f0e-d5fe2f657c1b"

			IRestResponse response3 = client3.Execute (requestN4U3);

			if (response3.StatusCode == System.Net.HttpStatusCode.OK)
			{
				notRetrive = false;
				Console.WriteLine(response3.Content);
				ju.SpacchettamentoJsonPremi(ListPunti, ListBuono, response3.Content);
			}
			else
			{
				notRetrive = true;

				ListPunti.Add(0);
				ListBuono.Add(0);

				ListPunti.Add(0);
				ListBuono.Add(0);

				ListPunti.Add(0);
				ListBuono.Add(0);

				ListPunti.Add(0);
				ListBuono.Add(0);

				ListPunti.Add(0);
				ListBuono.Add(0);

				ListPunti.Add(0);
				ListBuono.Add(0);
			}

			//*************************************


			//image.ClearAnimation();
			Activity.RunOnUiThread(() => {
				view.FindViewById<RelativeLayout> (Resource.Id.relativeLayout1).Visibility = ViewStates.Visible;
				view.FindViewById<RelativeLayout> (Resource.Id.relativeLayout5).Visibility = ViewStates.Invisible;
				 // this works!
			
				//view.FindViewById<RelativeLayout> (Resource.Id.relativeLayout1).Visibility = ViewStates.Visible;
				//view.FindViewById<RelativeLayout> (Resource.Id.relativeLayout5).Visibility = ViewStates.Invisible;

				density = Resources.DisplayMetrics.Density;

				Typeface Roboto = Typeface.CreateFromAsset(Activity.Assets,"fonts/Roboto_Regular.ttf");
				Typeface Bubble = Typeface.CreateFromAsset(Activity.Assets,"fonts/Bubblegum.ttf");

				view.FindViewById<TextView> (Resource.Id.textView1).Typeface=Bubble;

				TextView AccediTab = view.FindViewById<TextView> (Resource.Id.AccediTab);
				AccediTab.Typeface = Roboto;
				TextView AcquistiTab = view.FindViewById<TextView> (Resource.Id.AcquistiTab);
				AcquistiTab.Typeface = Roboto;
				TextView PremiTab = view.FindViewById<TextView> (Resource.Id.PremiTab);
				PremiTab.Typeface = Roboto;

				AccediTab.Click += delegate {
					AccediTab.SetBackgroundResource(Resource.Drawable.RoundedTabActive);
					AccediTab.SetTextColor(Color.Rgb(233,69,141));
					AcquistiTab.SetBackgroundResource(Resource.Drawable.RoundedTabDeactive);
					AcquistiTab.SetTextColor(Color.White);
					PremiTab.SetBackgroundResource(Resource.Drawable.RoundedTabDeactive);
					PremiTab.SetTextColor(Color.White);

					ProfiloViewTab ();

				};
				AcquistiTab.Click += delegate {
					AccediTab.SetBackgroundResource(Resource.Drawable.RoundedTabDeactive);
					AccediTab.SetTextColor(Color.White);
					AcquistiTab.SetBackgroundResource(Resource.Drawable.RoundedTabActive);
					AcquistiTab.SetTextColor(Color.Rgb(233,69,141));
					PremiTab.SetBackgroundResource(Resource.Drawable.RoundedTabDeactive);
					PremiTab.SetTextColor(Color.White);

					AcquistiViewTab();
				};

				PremiTab.Click += delegate {
					AccediTab.SetBackgroundResource(Resource.Drawable.RoundedTabDeactive);
					AccediTab.SetTextColor(Color.White);
					AcquistiTab.SetBackgroundResource(Resource.Drawable.RoundedTabDeactive);
					AcquistiTab.SetTextColor(Color.White);
					PremiTab.SetBackgroundResource(Resource.Drawable.RoundedTabActive);
					PremiTab.SetTextColor(Color.Rgb(233,69,141));

					PremiViewTab();
				};

				ContentView = view.FindViewById<LinearLayout> (Resource.Id.ContentView);

			
				ProfiloViewTab ();
			});
			//ProfiloViewTab ();
		}

		public void ProfiloViewTab(){
			//= inflater.Inflate(Resource.Layout.ProfiloView,container,false);
			ContentView.RemoveAllViews();
			ContentView.AddView (ProfiloView);

			Typeface Bubble = Typeface.CreateFromAsset(Activity.Assets,"fonts/Bubblegum.ttf");
			Typeface Roboto = Typeface.CreateFromAsset(Activity.Assets,"fonts/Roboto_Regular.ttf");



			ImageView imageBarCode = ProfiloView.FindViewById<ImageView> (Resource.Id.QRCodeImage);
			if (EanTessera != "") {
				var barcodewriter = new ZXing.Mobile.BarcodeWriter {
					Format = ZXing.BarcodeFormat.CODE_128,
					Options = new ZXing.Common.EncodingOptions {
						Width = (int)(200 * density),
						Height = (int)(70 * density)
					} 
				};
				var barcode = barcodewriter.Write (EanTessera);

				imageBarCode.SetImageBitmap (barcode);
			} else {
				imageBarCode.Visibility = ViewStates.Gone;
			}
			ProfiloView.FindViewById<TextView> (Resource.Id.textView3).Typeface=Bubble;
			TesseraText = ProfiloView.FindViewById<TextView> (Resource.Id.TesseraLabel);
			TesseraText.Typeface=Roboto;
			TesseraText.Text = "N° carta " + EanTessera;
			PointText = ProfiloView.FindViewById<TextView> (Resource.Id.PointText);
			PointText.Typeface=Bubble;
			PointText.Text = saldoPunti.ToString();
			NomeText = ProfiloView.FindViewById<TextView> (Resource.Id.NomeText);
			NomeText.Typeface=Roboto;
			NomeText.Text = prefs.GetString ("NomeN4U", null);
			CognomeText = ProfiloView.FindViewById<TextView> (Resource.Id.CognomeText);
			CognomeText.Typeface=Roboto;
			CognomeText.Text = prefs.GetString ("CognomeN4U", null);

			int soldiBuono = 0;

			if (saldoPunti >= ListPunti[0])
			{
				soldiBuono++;
			}
			if (saldoPunti >= ListPunti[1])
			{
				soldiBuono++;
			}
			if (saldoPunti >= ListPunti[2])
			{
				soldiBuono++;
			}
			if (saldoPunti >= ListPunti[3])
			{
				soldiBuono++;
			}
			if (saldoPunti >= ListPunti[4])
			{
				soldiBuono++;
			}

			if (soldiBuono > 0) {
				ProfiloView.FindViewById<ImageView>(Resource.Id.imageView3).Visibility=ViewStates.Visible;
				ProfiloView.FindViewById<TextView> (Resource.Id.textView4).Visibility=ViewStates.Visible;
				ProfiloView.FindViewById<TextView> (Resource.Id.textView5).Visibility=ViewStates.Visible;
				ProfiloView.FindViewById<TextView> (Resource.Id.textView6).Visibility=ViewStates.Visible;
				ProfiloView.FindViewById<TextView> (Resource.Id.textView6).Text = "€ "+ListBuono[soldiBuono-1]+",00";

			} else {
				ProfiloView.FindViewById<ImageView>(Resource.Id.imageView3).Visibility=ViewStates.Gone;
				ProfiloView.FindViewById<TextView> (Resource.Id.textView4).Visibility=ViewStates.Gone;
				ProfiloView.FindViewById<TextView> (Resource.Id.textView5).Visibility=ViewStates.Gone;
				ProfiloView.FindViewById<TextView> (Resource.Id.textView6).Visibility=ViewStates.Gone;
			}



		}

		public void AcquistiViewTab(){
			//= inflater.Inflate(Resource.Layout.ProfiloView,container,false);
			ContentView.RemoveAllViews ();
			ContentView.AddView (AcquistiView);

			Typeface Roboto = Typeface.CreateFromAsset(Activity.Assets,"fonts/Roboto_Regular.ttf");
			Typeface Bubble = Typeface.CreateFromAsset(Activity.Assets,"fonts/Bubblegum.ttf");

			if (ListAquistiObject.Count == 0) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.relativeLayout3).Visibility = ViewStates.Gone;
				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.relativeLayout4).Visibility = ViewStates.Visible;
				AcquistiView.FindViewById<TextView> (Resource.Id.textView1).Typeface = Bubble;
			} else {
				
				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.relativeLayout4).Visibility = ViewStates.Gone;
				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.relativeLayout3).Visibility = ViewStates.Visible;

				if (ListAquistiObject.Count > 0) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo1).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo1).Text = ListAquistiObject [0].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo1).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo1).Text = ListAquistiObject [0].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view1).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo1).Click += delegate {
						MostraDettagliView (0);
					};
				}

				if (ListAquistiObject.Count > 1) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo2).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo2).Text = ListAquistiObject [1].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo2).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo2).Text = ListAquistiObject [1].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view2).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo2).Click += delegate {
						MostraDettagliView (1);
					};
				}

				if (ListAquistiObject.Count > 2) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo3).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo3).Text = ListAquistiObject [2].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo3).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo3).Text = ListAquistiObject [2].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view3).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo3).Click += delegate {
						MostraDettagliView (2);
					};
				}

				if (ListAquistiObject.Count > 3) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo4).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo4).Text = ListAquistiObject [3].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo4).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo4).Text = ListAquistiObject [3].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view4).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo4).Click += delegate {
						MostraDettagliView (3);
					};
				}

				if (ListAquistiObject.Count > 4) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo5).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo5).Text = ListAquistiObject [4].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo5).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo5).Text = ListAquistiObject [4].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view5).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo5).Click += delegate {
						MostraDettagliView (4);
					};
				}

				if (ListAquistiObject.Count > 5) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo6).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo6).Text = ListAquistiObject [5].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo6).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo6).Text = ListAquistiObject [5].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view6).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo6).Click += delegate {
						MostraDettagliView (5);
					};
				}

				if (ListAquistiObject.Count > 6) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo7).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo7).Text = ListAquistiObject [6].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo7).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo7).Text = ListAquistiObject [6].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view7).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo7).Click += delegate {
						MostraDettagliView (6);
					};
				}

				if (ListAquistiObject.Count > 7) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo8).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo8).Text = ListAquistiObject [7].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo8).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo8).Text = ListAquistiObject [7].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view8).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo8).Click += delegate {
						MostraDettagliView (7);
					};
				}

				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo9).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo9).Text = ListAquistiObject [8].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo9).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo9).Text = ListAquistiObject [8].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view9).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo9).Click += delegate {
						MostraDettagliView (8);
					};
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo10).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.nomeArticolo10).Text = ListAquistiObject [9].Nome;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo10).Typeface = Roboto;
					AcquistiView.FindViewById<TextView> (Resource.Id.prezzoArticolo10).Text = ListAquistiObject [9].Prezzo + "€";
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					AcquistiView.FindViewById<View> (Resource.Id.view10).SetBackgroundColor (Color.Rgb (0, 160, 210));
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.Articolo10).Click += delegate {
						MostraDettagliView (9);
					};
				}
			}

		}

		public void PremiViewTab(){
			//= inflater.Inflate(Resource.Layout.ProfiloView,container,false);
			ContentView.RemoveAllViews();
			ContentView.AddView (PremiView);

			Typeface Bubble = Typeface.CreateFromAsset(Activity.Assets,"fonts/Bubblegum.ttf");
			Typeface Roboto = Typeface.CreateFromAsset(Activity.Assets,"fonts/Roboto_Regular.ttf");

			PremiView.FindViewById<TextView> (Resource.Id.Label).Typeface=Bubble;
			PremiView.FindViewById<TextView> (Resource.Id.textView1).Typeface=Roboto;
			PremiView.FindViewById<TextView> (Resource.Id.PetaliPoint).Typeface=Roboto;
			PremiView.FindViewById<TextView> (Resource.Id.textView2).Typeface=Roboto;
			PremiView.FindViewById<TextView> (Resource.Id.BuonoPoint).Typeface=Roboto;

			TextView PetaliPoint = PremiView.FindViewById<TextView> (Resource.Id.PetaliPoint);
			TextView BuonoPoint = PremiView.FindViewById<TextView> (Resource.Id.BuonoPoint);
			PetaliPoint.Text = ListPunti[2].ToString();
			BuonoPoint.Text = "€ "+ListBuono[2].ToString()+",00";

			ImageView image1 = PremiView.FindViewById<ImageView> (Resource.Id.image1);
			ImageView image2 = PremiView.FindViewById<ImageView> (Resource.Id.image2);
			ImageView image3 = PremiView.FindViewById<ImageView> (Resource.Id.image3);
			ImageView image4 = PremiView.FindViewById<ImageView> (Resource.Id.image4);
			ImageView image5 = PremiView.FindViewById<ImageView> (Resource.Id.image5);

			LinearLayout BordoImage1 = PremiView.FindViewById<LinearLayout> (Resource.Id.ContornoImage1);
			LinearLayout BordoImage2 = PremiView.FindViewById<LinearLayout> (Resource.Id.ContornoImage2);
			LinearLayout BordoImage3 = PremiView.FindViewById<LinearLayout> (Resource.Id.ContornoImage3);
			LinearLayout BordoImage4 = PremiView.FindViewById<LinearLayout> (Resource.Id.ContornoImage4);
			LinearLayout BordoImage5 = PremiView.FindViewById<LinearLayout> (Resource.Id.ContornoImage5);

			View DavantiImage1 = PremiView.FindViewById<View> (Resource.Id.DavantiImage1);
			View DavantiImage2 = PremiView.FindViewById<View> (Resource.Id.DavantiImage2);
			View DavantiImage3 = PremiView.FindViewById<View> (Resource.Id.DavantiImage3);
			View DavantiImage4 = PremiView.FindViewById<View> (Resource.Id.DavantiImage4);
			View DavantiImage5 = PremiView.FindViewById<View> (Resource.Id.DavantiImage5);

			image1.Click += delegate {
				image1.SetBackgroundColor(Color.Rgb(237,237,237));
				BordoImage1.SetBackgroundColor(Color.Rgb(0,160,210));
				DavantiImage1.SetBackgroundColor(Color.Rgb(237,237,237));

				image2.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage2.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage2.SetBackgroundColor(Color.Rgb(0,160,210));

				image3.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage3.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage3.SetBackgroundColor(Color.Rgb(0,160,210));

				image4.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage4.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage4.SetBackgroundColor(Color.Rgb(0,160,210));

				image5.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage5.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage5.SetBackgroundColor(Color.Rgb(0,160,210));

				PetaliPoint.Text = ListPunti[0].ToString();
				BuonoPoint.Text = "€ "+ListBuono[0].ToString()+",00";

				image1.SetImageResource(Resource.Drawable.Margherita_attivo_1);
				image2.SetImageResource(Resource.Drawable.Margerita_spento_2);
				image3.SetImageResource(Resource.Drawable.Margerita_spento_3);
				image4.SetImageResource(Resource.Drawable.Margerita_spento_4);
				image5.SetImageResource(Resource.Drawable.Margerita_spento_5);

			};

			image2.Click += delegate {
				image2.SetBackgroundColor(Color.Rgb(237,237,237));
				BordoImage2.SetBackgroundColor(Color.Rgb(0,160,210));
				DavantiImage2.SetBackgroundColor(Color.Rgb(237,237,237));

				image1.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage1.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage1.SetBackgroundColor(Color.Rgb(0,160,210));

				image3.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage3.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage3.SetBackgroundColor(Color.Rgb(0,160,210));

				image4.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage4.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage4.SetBackgroundColor(Color.Rgb(0,160,210));

				image5.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage5.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage5.SetBackgroundColor(Color.Rgb(0,160,210));

				PetaliPoint.Text = ListPunti[1].ToString();
				BuonoPoint.Text = "€ "+ListBuono[1].ToString()+",00";

				image2.SetImageResource(Resource.Drawable.Margherita_attivo_2);
				image1.SetImageResource(Resource.Drawable.Margerita_spento_1);
				image3.SetImageResource(Resource.Drawable.Margerita_spento_3);
				image4.SetImageResource(Resource.Drawable.Margerita_spento_4);
				image5.SetImageResource(Resource.Drawable.Margerita_spento_5);

			};

			image3.Click += delegate {
				image3.SetBackgroundColor(Color.Rgb(237,237,237));
				BordoImage3.SetBackgroundColor(Color.Rgb(0,160,210));
				DavantiImage3.SetBackgroundColor(Color.Rgb(237,237,237));

				image2.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage2.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage2.SetBackgroundColor(Color.Rgb(0,160,210));

				image1.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage1.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage1.SetBackgroundColor(Color.Rgb(0,160,210));

				image4.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage4.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage4.SetBackgroundColor(Color.Rgb(0,160,210));

				image5.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage5.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage5.SetBackgroundColor(Color.Rgb(0,160,210));

				PetaliPoint.Text = ListPunti[2].ToString();
				BuonoPoint.Text = "€ "+ListBuono[2].ToString()+",00";

				image3.SetImageResource(Resource.Drawable.Margherita_attivo_3);
				image2.SetImageResource(Resource.Drawable.Margerita_spento_2);
				image1.SetImageResource(Resource.Drawable.Margerita_spento_1);
				image4.SetImageResource(Resource.Drawable.Margerita_spento_4);
				image5.SetImageResource(Resource.Drawable.Margerita_spento_5);

			};

			image4.Click += delegate {
				image4.SetBackgroundColor(Color.Rgb(237,237,237));
				BordoImage4.SetBackgroundColor(Color.Rgb(0,160,210));
				DavantiImage4.SetBackgroundColor(Color.Rgb(237,237,237));

				image2.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage2.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage2.SetBackgroundColor(Color.Rgb(0,160,210));

				image3.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage3.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage3.SetBackgroundColor(Color.Rgb(0,160,210));

				image1.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage1.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage1.SetBackgroundColor(Color.Rgb(0,160,210));

				image5.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage5.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage5.SetBackgroundColor(Color.Rgb(0,160,210));

				PetaliPoint.Text = ListPunti[3].ToString();
				BuonoPoint.Text = "€ "+ListBuono[3].ToString()+",00";

				image4.SetImageResource(Resource.Drawable.Margherita_attivo_4);
				image2.SetImageResource(Resource.Drawable.Margerita_spento_2);
				image3.SetImageResource(Resource.Drawable.Margerita_spento_3);
				image1.SetImageResource(Resource.Drawable.Margerita_spento_1);
				image5.SetImageResource(Resource.Drawable.Margerita_spento_5);

			};

			image5.Click += delegate {
				image5.SetBackgroundColor(Color.Rgb(237,237,237));
				BordoImage5.SetBackgroundColor(Color.Rgb(0,160,210));
				DavantiImage5.SetBackgroundColor(Color.Rgb(237,237,237));

				image2.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage2.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage2.SetBackgroundColor(Color.Rgb(0,160,210));

				image3.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage3.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage3.SetBackgroundColor(Color.Rgb(0,160,210));

				image4.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage4.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage4.SetBackgroundColor(Color.Rgb(0,160,210));

				image1.SetBackgroundColor(Color.Rgb(178,178,178));
				BordoImage1.SetBackgroundColor(Color.Rgb(178,178,178));
				DavantiImage1.SetBackgroundColor(Color.Rgb(0,160,210));

				PetaliPoint.Text = ListPunti[4].ToString();
				BuonoPoint.Text = "€ "+ListBuono[4].ToString()+",00";

				image5.SetImageResource(Resource.Drawable.Margherita_attivo_5);
				image2.SetImageResource(Resource.Drawable.Margerita_spento_2);
				image3.SetImageResource(Resource.Drawable.Margerita_spento_3);
				image4.SetImageResource(Resource.Drawable.Margerita_spento_4);
				image1.SetImageResource(Resource.Drawable.Margerita_spento_1);

			};
		}

		public void MostraDettagliView(int indice){

			if (indice == 0) {

				if (ListAquistiObject.Count > 1) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
					flagA2 = false;
				}

				if (ListAquistiObject.Count > 2) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
					flagA3 = false;
				}

				if (ListAquistiObject.Count > 3) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
					flagA4 = false;
				}

				if (ListAquistiObject.Count > 4) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
					flagA5 = false;
				}

				if (ListAquistiObject.Count > 5) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
					flagA6 = false;
				}

				if (ListAquistiObject.Count > 6) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
					flagA7 = false;
				}

				if (ListAquistiObject.Count > 7) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					flagA8 = false;
				}

				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA1 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA1 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
					flagA1 = false;
				}
			}

			if (indice == 1) {
			
				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				if (ListAquistiObject.Count > 2) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
					flagA3 = false;
				}

				if (ListAquistiObject.Count > 3) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
					flagA4 = false;
				}

				if (ListAquistiObject.Count > 4) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
					flagA5 = false;
				}

				if (ListAquistiObject.Count > 5) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
					flagA6 = false;
				}

				if (ListAquistiObject.Count > 6) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
					flagA7 = false;
				}

				if (ListAquistiObject.Count > 7) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					flagA8 = false;
				}

				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA2 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA2 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
					flagA2 = false;
				}

			}

			if (indice == 2) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
				flagA2 = false;


				if (ListAquistiObject.Count > 3) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
					flagA4 = false;
				}

				if (ListAquistiObject.Count > 4) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
					flagA5 = false;
				}

				if (ListAquistiObject.Count > 5) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
					flagA6 = false;
				}

				if (ListAquistiObject.Count > 6) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
					flagA7 = false;
				}

				if (ListAquistiObject.Count > 7) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					flagA8 = false;
				}

				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA3 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA3 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
					flagA3 = false;
				}

			}
				

			if (indice == 3) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
				flagA2 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
				flagA3 = false;


				if (ListAquistiObject.Count > 4) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
					flagA5 = false;
				}

				if (ListAquistiObject.Count > 5) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
					flagA6 = false;
				}

				if (ListAquistiObject.Count > 6) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
					flagA7 = false;
				}

				if (ListAquistiObject.Count > 7) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					flagA8 = false;
				}

				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA4 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA4 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
					flagA4 = false;
				}

			}

			if (indice == 4) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
				flagA2 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
				flagA3 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
				flagA4 = false;


				if (ListAquistiObject.Count > 5) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
					flagA6 = false;
				}

				if (ListAquistiObject.Count > 6) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
					flagA7 = false;
				}

				if (ListAquistiObject.Count > 7) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					flagA8 = false;
				}

				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA5 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA5 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
					flagA5 = false;
				}

			}

			if (indice == 5) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
				flagA2 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
				flagA3 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
				flagA4 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
				flagA5 = false;


				if (ListAquistiObject.Count > 6) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
					flagA7 = false;
				}

				if (ListAquistiObject.Count > 7) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					flagA8 = false;
				}

				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA6 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA6 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
					flagA6 = false;
				}

			}

			if (indice == 6) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
				flagA2 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
				flagA3 = false;
			
				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
				flagA4 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
				flagA5 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
				flagA6 = false;


				if (ListAquistiObject.Count > 7) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					flagA8 = false;
				}

				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA7 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA7 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
					flagA7 = false;
				}

			}

			if (indice == 7) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
				flagA2 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
				flagA3 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
				flagA4 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
				flagA5 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
				flagA6 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
				flagA7 = false;


				if (ListAquistiObject.Count > 8) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA8 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA8 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
					flagA8 = false;
				}

			}

			if (indice == 8) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
				flagA2 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
				flagA3 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
				flagA4 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
				flagA5 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
				flagA6 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
				flagA7 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
				flagA8 = false;


				if (ListAquistiObject.Count > 9) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

				if (flagA9 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA9 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
					flagA9 = false;
				}

			}

			if (indice == 9) {

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo1).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo1).SetImageResource (Resource.Drawable.freccia_giu);
				flagA1 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo2).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo2).SetImageResource (Resource.Drawable.freccia_giu);
				flagA2 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo3).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo3).SetImageResource (Resource.Drawable.freccia_giu);
				flagA3 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo4).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo4).SetImageResource (Resource.Drawable.freccia_giu);
				flagA4 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo5).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo5).SetImageResource (Resource.Drawable.freccia_giu);
				flagA5 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo6).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo6).SetImageResource (Resource.Drawable.freccia_giu);
				flagA6 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo7).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo7).SetImageResource (Resource.Drawable.freccia_giu);
				flagA7 = false;

				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo8).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo8).SetImageResource (Resource.Drawable.freccia_giu);
				flagA8 = false;
		
				AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo9).RemoveAllViews ();
				AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo9).SetImageResource (Resource.Drawable.freccia_giu);
				flagA9 = false;


				if (flagA10 == false) {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).AddView (DettagliAcquistiView);
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_su);
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.PVLabel).Text = ListAquistiObject [indice].PV;
					DettagliAcquistiView.FindViewById<TextView> (Resource.Id.DataLabel).Text = ListAquistiObject [indice].Data;
					flagA10 = true;
				} else {
					AcquistiView.FindViewById<RelativeLayout> (Resource.Id.DescArticolo10).RemoveAllViews ();
					AcquistiView.FindViewById<ImageView> (Resource.Id.frecciaArticolo10).SetImageResource (Resource.Drawable.freccia_giu);
					flagA10 = false;
				}

			}

		}
	}
}

