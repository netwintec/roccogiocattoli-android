﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Webkit;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using Android.Views.Animations;

namespace RoccoGiocattoli
{
	public class FragmentNews : Fragment
	{
		public WebView webview;
		public ImageView image;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentNews, container, false);

			FragmentNews.Instance = this;

			webview = view.FindViewById<WebView> (Resource.Id.webNews);
			webview.Settings.JavaScriptEnabled = true;
			webview.SetWebViewClient (new MyWebViewClient2());
			webview.LoadUrl ("http://www.roccogiocattoli.eu/news-app/");
			webview.Settings.CacheMode = CacheModes.CacheElseNetwork;

			image = view.FindViewById<ImageView> (Resource.Id.imageView1);

			var rotateAboutCenterAnimation = AnimationUtils.LoadAnimation(Activity.ApplicationContext,Resource.Animation.RotationAnim);
			image.StartAnimation(rotateAboutCenterAnimation);
			return view;
		}

		public static FragmentNews Instance {get;private set;}
	}

	class MyWebViewClient2 : WebViewClient
	{
		Boolean loadingFinished = true;
		Boolean redirect = false;

		public MyWebViewClient2(){
		}
		public override bool ShouldOverrideUrlLoading(WebView view, string url)
		{
			if (!loadingFinished)
			{
				redirect = true;
			}

			loadingFinished = false;
			view.LoadUrl(url);
			Console.WriteLine("Loading web view...");
			return true;
		}

		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			base.OnPageStarted(view, url, favicon);
			loadingFinished = false;
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (!redirect)
			{
				loadingFinished = true;
			}

			if (loadingFinished && !redirect) 
			{
				Console.WriteLine("Finished Loading!!!");
				FragmentNews.Instance.webview.LayoutParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent,RelativeLayout.LayoutParams.WrapContent);
				FragmentNews.Instance.image.Visibility = ViewStates.Invisible;
			}
			else
			{
				redirect = false;
			}
		}

	}
}

