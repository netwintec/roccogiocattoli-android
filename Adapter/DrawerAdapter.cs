﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RoccoGiocattoli
{
	public class DrawerAdapter : BaseAdapter
	{
		Activity context;

		public List<ListObject> items;

		public DrawerAdapter(Activity context) : base()
		{
			this.context = context;

			//For demo purposes we hard code some data here
			this.items = new List<ListObject>() {
				new ListObject() { First = true},
				new ListObject() { Text = "Home",Image = Resource.Drawable.home_icon_list },
				new ListObject() { Text = "Rocco Giocattoli", Image = Resource.Drawable.rocco_icon_list },
				new ListObject() { Text = "Il Mio Profilo", Image = Resource.Drawable.profilo_icon_list },
				new ListObject() { Text = "News", Image = Resource.Drawable.news_icon_list },
				new ListObject() { Text = "Punti Vendita", Image = Resource.Drawable.negzzi_icon_list },
				new ListObject() { Text = "Contattaci", Image = Resource.Drawable.mail_icon_list },
				new ListObject() { Text = "Logout", Image = Resource.Drawable.logout_icon_list },
			};
		}

		public override int Count
		{
			get { return items.Count; }
		}

		public override Java.Lang.Object GetItem(int position)
		{
			return position;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//Get our object for this position
			//var item = items[position];  

			View view = convertView;


			Typeface Bubblegum = Typeface.CreateFromAsset(this.context.Assets,"fonts/Bubblegum.ttf");

			if(position == 0){
				LayoutInflater inflater = ((Activity) context).LayoutInflater;

				view = inflater.Inflate(Resource.Layout.drawer_logo, parent, false);
				//((ImageView) view.FindViewById<ImageView<(R.id.LogoImage)).setImageDrawable(view.getResources().getDrawable(R.drawable.logo));

				return view;

			}else {

				LayoutInflater inflater = ((Activity) context).LayoutInflater;

				ListObject dItem = this.items[position];

				view = inflater.Inflate(Resource.Layout.drawer_option, parent, false);

				view.FindViewById<ImageView>(Resource.Id.optionImage).SetImageResource(dItem.Image);
				view.FindViewById<TextView>(Resource.Id.optionText).Text=dItem.Text;
				view.FindViewById<TextView>(Resource.Id.optionText).Typeface=Bubblegum;

				return view;
			}

			return view;
		}

		public ListObject GetItemAtPosition(int position)
		{
			return items[position];
		}
	}


	public class ListObject
	{
		public int Image
		{
			get;
			set;
		}

		public string Text
		{
			get;
			set;
		}

		public bool First
		{
			get;
			set;
		}
	}
}

