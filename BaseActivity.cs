﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Content.PM;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;

namespace RoccoGiocattoli
{
	[Activity (Label = "BaseActivity",ScreenOrientation = ScreenOrientation.Portrait)]			
	public abstract class BaseActivity : ActionBarActivity
	{
		private Toolbar toolbar;

		private ImageView menuIcon,backIcon;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView(getLayoutResource());

			toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
			if (toolbar != null) {
				SetSupportActionBar(toolbar);
				SupportActionBar.SetDisplayHomeAsUpEnabled(false);
				SupportActionBar.SetDisplayShowTitleEnabled(false);
			}

			menuIcon = (ImageView) toolbar.FindViewById(Resource.Id.home_icon);
			backIcon = (ImageView) toolbar.FindViewById(Resource.Id.back_icon);
		}

		protected abstract int getLayoutResource();

		protected void setActionBarIcon(int iconRes,int iconRes2) {
			menuIcon.SetImageResource(iconRes);
			backIcon.SetImageResource(iconRes2);
		}

		public Toolbar getToolbar(){
			return toolbar;
		}
	}

}

