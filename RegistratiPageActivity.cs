﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Android.Support.V4.App;
using Android.Content.PM;
using Java.Security;
using Xamarin.Facebook.AppEvents;
using Android.Graphics;
using Android.Views.Animations;

namespace RoccoGiocattoli
{
	[Activity (Label = "@string/app_name", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class RegistratiPageActivity : Activity
	{

		Button Registrati;
		ICallbackManager callbackManager;
		LoginButton loginButton;
		EditText Nome,Cognome,Email,Password;
		UtilityLoginManager loginManager = new UtilityLoginManager();
		ImageView imageRotante;
		RelativeLayout Loading;
		Animation rotateAboutCenterAnimation;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);


			callbackManager = CallbackManagerFactory.Create ();

			var loginCallback = new FacebookCallback<LoginResult> {

				HandleSuccess = loginResult => {
					//login
					Console.WriteLine ("Loggato con successo");
					Console.WriteLine(AccessToken.CurrentAccessToken.Token);

					RunOnUiThread (() => {
						Loading.Visibility = ViewStates.Visible;
						imageRotante.RefreshDrawableState();
					} );

					List<string> result = loginManager.LoginFB(AccessToken.CurrentAccessToken.Token);

					if(result[0] =="Errore"){

						RunOnUiThread (() => {
							Loading.Visibility = ViewStates.Invisible;
							imageRotante.RefreshDrawableState();
						} );

						LoginManager.Instance.LogOut();
						AlertDialog.Builder alert = new AlertDialog.Builder (this);
						alert.SetTitle("Errore di rete");
						alert.SetMessage ("Login non Riuscito riprovare più tardi");
						alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
						alert.Show();
					}

					else{

						Console.WriteLine(result [0]+"  "+result[1]+"  "+result [2]+"  "+result[3]+"  "+result [4]);
						if(result[4]==""){
							var prefs = MainActivity.Instance.prefs;
							var prefEditor = prefs.Edit();
							prefEditor.PutString("AppoggioTokenN4U",result[1]);
							prefEditor.PutString("NomeN4U",result[2]);
							prefEditor.PutString("CognomeN4U",result[3]);
							prefEditor.Commit();
							var intent = new Intent (this, typeof(CardActivity));
							StartActivity (intent);
							Finish();
						}else {
							var prefs = MainActivity.Instance.prefs;
							var prefEditor = prefs.Edit();
							prefEditor.PutString("TokenN4U",result[1]);
							prefEditor.PutString("NomeN4U",result[2]);
							prefEditor.PutString("CognomeN4U",result[3]);
							prefEditor.Commit();
							var intent = new Intent (this, typeof(HomePageActivity));
							StartActivity (intent);
							Finish();
						}
					}

				},
				HandleCancel = () => {
					Console.WriteLine ("Condizioni non accettate");
				},
				HandleError = loginError => {
					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Errore di rete");
					alert.SetMessage ("Login non Riuscito riprovare più tardi");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();
				}
			};

			LoginManager.Instance.RegisterCallback (callbackManager, loginCallback);

			//Regulag Login

			SetContentView (Resource.Layout.RegistratiPage);
			ActionBar.Hide ();

			imageRotante = FindViewById<ImageView> (Resource.Id.logoRotante);
			Loading = FindViewById<RelativeLayout> (Resource.Id.loading);

			rotateAboutCenterAnimation = AnimationUtils.LoadAnimation(this,Resource.Animation.RotationAnim);
			imageRotante.StartAnimation(rotateAboutCenterAnimation);
			Loading.Visibility = ViewStates.Invisible;


			loginButton = (LoginButton) FindViewById<Button> (Resource.Id.LoginFb);

			loginButton.SetReadPermissions("public_profile","email");

			Typeface Roboto = Typeface.CreateFromAsset(Assets,"fonts/Roboto_Regular.ttf"); 

			Registrati = FindViewById<Button> (Resource.Id.RegistratiButton);
			Registrati.Typeface = Roboto;

			Registrati.Click += delegate {

				RunOnUiThread (() => {
					Loading.Visibility = ViewStates.Visible;
					imageRotante.RefreshDrawableState();
				} );

				List <string> value=loginManager.ValidationReg(Nome.Text,Cognome.Text,Email.Text,Password.Text);
				int errore=0;
				if(value[0]=="ERRORDATA"){
					
					RunOnUiThread (() => {
						Loading.Visibility = ViewStates.Invisible;
						imageRotante.RefreshDrawableState();
					} );

					Console.WriteLine(value[1]);
					if(value[1].Contains("N"))
						errore++;
					if(value[1].Contains("C"))
						errore++;
					if(value[1].Contains("P"))
						errore++;
					if(value[1].Contains("E")){
						errore++;
						Email.Text="";
					}
					if(errore == 1){
						AlertDialog.Builder alert = new AlertDialog.Builder (this);
						alert.SetTitle("Errore");
						alert.SetMessage ("Errore 1 campo da lei inserito contiene valori errati o non contiene nessun valore, perfavore controlli");
						alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
						alert.Show();
					}
					if(errore >=2){
						AlertDialog.Builder alert = new AlertDialog.Builder (this);
						alert.SetTitle("Errore");
						alert.SetMessage ("Errore 2 o più campi  da lei inseriti contengono valori errati o non contengono nessun valore, perfavore controlli");
						alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
						alert.Show();
						}
				}
				if(value[0] == "SUCCESS"){
					Console.WriteLine(value [0]+"  "+value[1]+"  "+value [2]);
					if(value[2]==""){
						var prefs = MainActivity.Instance.prefs;
						var prefEditor = prefs.Edit();
						prefEditor.PutString("AppoggioTokenN4U",value[1]);
						prefEditor.PutString("NomeN4U",Nome.Text);
						prefEditor.PutString("CognomeN4U",Cognome.Text);
						prefEditor.Commit();
						var intent = new Intent (this, typeof(CardActivity));
						StartActivity (intent);
						Finish();
					}else {
						var prefs = MainActivity.Instance.prefs;
						var prefEditor = prefs.Edit();
						prefEditor.PutString("TokenN4U",value[1]);
						prefEditor.PutString("NomeN4U",Nome.Text);
						prefEditor.PutString("CognomeN4U",Cognome.Text);
						prefEditor.Commit();
						var intent = new Intent (this, typeof(HomePageActivity));
						StartActivity (intent);
						Finish();
					}
				}

				if(value[0] == "ERROR"){

					RunOnUiThread (() => {
						Loading.Visibility = ViewStates.Invisible;
						imageRotante.RefreshDrawableState();
					} );

					Nome.Text="";
					Cognome.Text="";
					Email.Text="";
					Password.Text="";
					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Registrazione Non Riuscita");
					alert.SetMessage ("Errore di Rete");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();

				}

				if(value[0] == "ERROREXIST"){

					RunOnUiThread (() => {
						Loading.Visibility = ViewStates.Invisible;
						imageRotante.RefreshDrawableState();
					} );
						
					Email.Text="";
					Password.Text="";
					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle("Registrazione Non Riuscita");
					alert.SetMessage ("Email già esistente");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {} );
					alert.Show();

				}


			};
			FindViewById<TextView> (Resource.Id.textView1).Typeface = Roboto;
			FindViewById<TextView> (Resource.Id.textView2).Typeface = Roboto;
			FindViewById<TextView> (Resource.Id.textView3).Typeface = Roboto;
			Nome = FindViewById<EditText> (Resource.Id.NomeText);
			Nome.Typeface = Roboto;
			Cognome = FindViewById<EditText> (Resource.Id.CognomeText);
			Cognome.Typeface = Roboto;
			Email = FindViewById<EditText> (Resource.Id.EmailText);
			Email.Typeface = Roboto;
			Password = FindViewById<EditText> (Resource.Id.PasswordText);
			Password.Typeface = Roboto;
			// Create your application here
		}
			

		public override void OnBackPressed ()
		{
			MainActivity.Instance.CountBluetooth = -1;
			base.OnBackPressed ();
		}

		protected override void OnSaveInstanceState (Bundle outState)
		{
			base.OnSaveInstanceState (outState);
		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);

			callbackManager.OnActivityResult (requestCode, (int)resultCode, data);
		}
			

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			AppEventsLogger.DeactivateApp (this);

			Console.WriteLine ("ONPAUSE1 "+MainActivity.Instance.Paused);
			MainActivity.Instance.Paused = true;
			Console.WriteLine ("ONPAUSE2 "+MainActivity.Instance.Paused);
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			AppEventsLogger.ActivateApp (this);

			Console.WriteLine ("ONRESUME1 "+MainActivity.Instance.Paused);
			MainActivity.Instance.Paused = false;
			Console.WriteLine ("ONRESUME2 "+MainActivity.Instance.Paused);
		}

	}
}

