﻿using System;
using System.Json;
using System.Linq;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using RestSharp;
using ZXing.Mobile;
using ZXing;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Gcm.Client;
using Android.Util;
using RadiusNetworks.IBeaconAndroid;
using Android.Bluetooth;
using Android.Support.V4.App;
                               

namespace RoccoGiocattoli
{
	[Activity (ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity,IBeaconConsumer
	{
		private const string UUID = "ACFD065E-C3C0-11E3-9BBE-1A514932AC01";
		private const string beaconId = "BlueBeacon";

		IBeaconManager _iBeaconManager;
		MonitorNotifier _monitorNotifier;
		RangeNotifier _rangeNotifier;
		RadiusNetworks.IBeaconAndroid.Region _monitoringRegion;
		RadiusNetworks.IBeaconAndroid.Region _rangingRegion;
		Dictionary <string,bool> flagBeacon = new Dictionary <string,bool> ();
		public bool flagTokenN4U = true;
		public Dictionary<string,Beacon> ListBeacon = new Dictionary<string,Beacon> ();
		public List<Messaggio> ListMessaggi = new List <Messaggio> ();
		Button Login,Registrati;
		public Dictionary<int,XmlObject> XmlObjectDictionary = new Dictionary<int,XmlObject>();
		public int totalValue;
		public ISharedPreferences prefs;
		int porta =81,i=0;
		public string PushToken;
		public string TokenN4U;
		public bool Scan;
		public int CountBluetooth =0;

		public bool Paused = false;

		public bool Notification;
		public string ImageUrl,Descrizione;
		public string PushKey;
		public string Key;

		public bool BluetoothAlert = false;

		public MainActivity()
		{
			_iBeaconManager = IBeaconManager.GetInstanceForApplication(this);

			_monitorNotifier = new MonitorNotifier();
			_rangeNotifier = new RangeNotifier();

			_monitoringRegion = new RadiusNetworks.IBeaconAndroid.Region(beaconId, UUID, null, null);
			_rangingRegion = new RadiusNetworks.IBeaconAndroid.Region(beaconId, UUID, null, null);

		}

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Notification = Intent.GetBooleanExtra ("Notification", false);
			Descrizione = Intent.GetStringExtra ("descrizione");
			ImageUrl = Intent.GetStringExtra ("image");
			PushKey = Intent.GetStringExtra ("PushKey");

			//Check to ensure everything's setup right
			GcmClient.CheckDevice(this);
			GcmClient.CheckManifest(this);
			GcmClient.Register(this, GcmBroadcastReceiver.SENDER_IDS);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			MobileBarcodeScanner.Initialize (Application);
			FacebookSdk.SdkInitialize (Application);

			ActionBar.Hide ();

			MainActivity.Instance = this;

			Console.WriteLine ("Count" + CountBluetooth);

			prefs = Application.Context.GetSharedPreferences ("RoccoGiocattoli", FileCreationMode.Private);
			var Token = prefs.GetString ("TokenN4U", null);
			if (Token != null || Token != "") {

				Console.WriteLine ("TOKEN N4U:" + Token);
				var client = new RestClient ("http://api.netwintec.com:" + porta + "/");
				//client.Authenticator = new HttpBasicAuthenticator(username, password);

				var requestN4U = new RestRequest ("profile", Method.GET);
				requestN4U.AddHeader ("content-type", "application/json");
				requestN4U.AddHeader ("Net4U-Company", "roccogiocattoli");
				requestN4U.AddHeader ("Net4U-Token", prefs.GetString ("TokenN4U", null));  // "e74e8d8c-b4c5-482b-9f0e-d5fe2f657c1b"

				IRestResponse response = client.Execute (requestN4U);
				Console.WriteLine ("RESPONSE CODE:" + response.StatusCode);
				if (response.StatusCode == System.Net.HttpStatusCode.OK) {
					var intent = new Intent (this, typeof(HomePageActivity));
					StartActivity (intent);
					CountBluetooth+=2;
				} else {
					LoginManager.Instance.LogOut ();
					CountBluetooth++;
				}
					
			} else {
				LoginManager.Instance.LogOut ();
				CountBluetooth++;

				if (Notification) {
					Console.WriteLine ("NOridication1:"+Notification);
					var intent = new Intent (this, typeof(NotificationActivity));
					intent.PutExtra ("Notification",true);
					this.StartActivity (intent);

				}
			}


			Console.WriteLine ("Count" + CountBluetooth);

			Typeface Roboto = Typeface.CreateFromAsset(Assets,"fonts/Roboto_Regular.ttf"); 

			Login = FindViewById<Button> (Resource.Id.ButtonLogin);
			Login.Typeface = Roboto;
			Registrati = FindViewById<Button> (Resource.Id.ButtonRegistrati);
			Registrati.Typeface = Roboto;
			Login.Click += delegate {
				var intent = new Intent (this, typeof(LoginPageActivity));
				StartActivity (intent);
			};

			Registrati.Click += delegate {
				var intent = new Intent(this, typeof(RegistratiPageActivity));
				StartActivity(intent);
			};


			// Start Beacon
			if (CountBluetooth == 1) {
				AlertDialog.Builder builder = new AlertDialog.Builder (this);
				builder.SetTitle ("Bluetooth");
				builder.SetMessage ("Se sei in un negozio attiva il bluethoot per magnifiche sorprese.");
				builder.SetCancelable (false);
				builder.SetPositiveButton ("OK", delegate {
				});

				BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;

				if (!bluetoothAdapter.IsEnabled) {
					//builder.Show ();
				}
			}


			var prefEditor = prefs.Edit();
			prefEditor.PutBoolean("Close",false);
			prefEditor.Commit();

			_iBeaconManager.Bind (this);

			_iBeaconManager.SetMonitorNotifier (_monitorNotifier);
			_iBeaconManager.SetRangeNotifier (_rangeNotifier);

			Console.WriteLine ("parto2");

			_rangeNotifier.DidRangeBeaconsInRegionComplete += RangingBeaconsInRegion;
		}

		public static MainActivity Instance {get;private set;}


		// ***************** BEACON TROVATO ***********************

		void RangingBeaconsInRegion(object sender, RangeEventArgs e){
			Console.WriteLine ("Token "+Scan);
			if (flagTokenN4U == true) {
				if (TokenN4U != null  && TokenN4U != "") {
					//
					var client = new RestClient ("http://api.netwintec.com:"+porta+"/");
					var requestMessage = new RestRequest ("active-messages", Method.GET);
					requestMessage.AddHeader ("content-type", "application/json");
					requestMessage.AddHeader ("Net4U-Company", "roccogiocattoli");
					requestMessage.AddHeader ("Net4U-Token", prefs.GetString ("TokenN4U", null));

					IRestResponse response = client.Execute (requestMessage);

					//string prova = "{\"messages\":[{\"title\":\"titolo\",\"description\":\"descrizione\",\"image_url\":\"/image/4fae906a-0e25-4f1d-953c-6d8cf1cc0219\",\"content_url\":\"url\",\"type\":\"ibeacon\",\"timestamp_start\":1429800825,\"timestamp_end\":1450882425,\"distance\":5,\"_type\":\"message\",\"_key\":\"message::e68461b7-13f9-4296-856e-3bf83ffd587a\"}]}";
					Console.WriteLine ("RESPONSE:"+response.Content);
					JsonValue json = JsonValue.Parse (response.Content);

					JsonValue data = json ["messages"];

					foreach (JsonValue dataItem in data) {
						var titolo = dataItem ["title"];
						var urlimg = dataItem ["image_url"];
						var descr = dataItem ["description"];
						var action = dataItem ["content_url"];
						var distanzaAp = dataItem ["threshold"];
						float distanzaFl=0;
						if (distanzaAp.ToString ().Contains (".")) {
							string distStr = distanzaAp.ToString ().Replace ('.', ',');
							distanzaFl = float.Parse (distStr);
						} else {
							distanzaFl = float.Parse (distanzaAp.ToString ());
						}
						Console.WriteLine (distanzaFl);
						int distanza = (int)(distanzaFl*100);
						Console.WriteLine (distanza);
						var key = dataItem ["_key"];

						string baseurl = "https://s3-eu-west-1.amazonaws.com/net4uimage/roccogiocattoli/";
						string urlimg2 = urlimg;
						string key2 = "";
						if (urlimg2.Contains ("image")) {
							key2 = urlimg2.Remove (0, 7);
							Console.WriteLine ("IMAGE_URL:"+baseurl + key2);
						} else {
							key2 = "null";
							baseurl ="";
							Console.WriteLine ("IMAGE_URL:"+baseurl + key2);
						}

						Console.WriteLine (titolo + "  " + baseurl + key2 + "   " + descr + "   " + action);

						ListMessaggi.Add (new Messaggio (titolo, baseurl + key2, descr, action,key, false));

						var client2 = new RestClient ("http://api.netwintec.com:"+porta+"/");
						var requestBeacon = new RestRequest ("active-messages/" + key, Method.GET);
						requestBeacon.AddHeader ("content-type", "application/json");
						requestBeacon.AddHeader ("Net4U-Company", "roccogiocattoli");
						requestBeacon.AddHeader ("Net4U-Token", prefs.GetString ("TokenN4U", null));

						IRestResponse response2 = client2.Execute (requestBeacon);

						Console.WriteLine (response2.Content);

						try
						{
							JsonValue json2 = JsonValue.Parse(response2.Content);
							JsonValue data2 = json2["beacons"];

							foreach (JsonValue dataItem2 in data2)
							{
								var uuid2 = dataItem2["uuid"];
								var majorAp = dataItem2["major"];
								int major = int.Parse(majorAp.ToString());
								var minorAp = dataItem2["minor"];
								int minor = int.Parse(minorAp.ToString());
								Console.WriteLine(uuid2 + "|" + major + "|" + minor + "|" + distanza);
								string Appoggio;
								Appoggio = major.ToString() + "," + minor.ToString() + "," + i;
								ListBeacon.Add(Appoggio, new Beacon(uuid2, major, minor, distanza, i));
								flagBeacon.Add(Appoggio, false);
								if (flagBeacon.ContainsKey(Appoggio) == true)
								{
									Console.WriteLine("key:" + Appoggio);
									Console.WriteLine("c'è");
								}
							}
						}
						catch (Exception e) { 
						
						}

						i++;
					}
					flagTokenN4U = false;
					Console.WriteLine ("Fine");
				}
			} else {
				if(Scan){
					if (e.Beacons.Count > 0) {
						var s2 = e.Beacons;
						foreach (IBeacon beacon in s2) {

							Console.WriteLine ("   " + beacon.Major.ToString () + "  " + beacon.Minor.ToString () + "  " + beacon.Rssi);
							string App2;
							List<string> BeacAppoggioCount = new List <string> ();

							int countBeac = 0;
							for (int q = 0; q < ListMessaggi.Count; q++) {
								App2 = beacon.Major.ToString () + "," + beacon.Minor.ToString () + "," + q;
								Console.WriteLine ("app2:" + beacon.Major.ToString () + "," + beacon.Minor.ToString () + "," + q + "|" + flagBeacon.ContainsKey (App2));
								if (flagBeacon.ContainsKey (App2) == true) {

									BeacAppoggioCount.Add (App2);
									countBeac++;

								}
							}
							Console.WriteLine (countBeac);
							if (countBeac != 0) {
								int a = 0;
								for (int w = 0; w < countBeac; w++) {
									Beacon BeaconApp;
									BeaconApp = ListBeacon [BeacAppoggioCount [w]];
									Console.WriteLine ("Visto" + BeaconApp.IdMessaggio + ":" + ListMessaggi [BeaconApp.IdMessaggio].Visto);
									if (ListMessaggi [BeaconApp.IdMessaggio].Visto == false) {
										int distRssi = 0;

										if (BeaconApp.Distance <= 100)
											distRssi = (int)(-60);

										if (BeaconApp.Distance > 100 && BeaconApp.Distance <= 500)
											distRssi = (int)(-67);

										if (BeaconApp.Distance > 500 && BeaconApp.Distance <= 1500)
											distRssi = (int)(-83);

										if (BeaconApp.Distance > 1500)
											distRssi = (int)(-90);

										if (beacon.Rssi > distRssi) {
											Messaggio MessaggioApp;
											MessaggioApp = ListMessaggi [BeaconApp.IdMessaggio];
											flagBeacon [BeacAppoggioCount [w]] = true;
											ListMessaggi [BeaconApp.IdMessaggio].Visto = true;


											var client = new RestClient("http://api.netwintec.com:" + porta + "/");
											//client.Authenticator = new HttpBasicAuthenticator(username, password);

											//var requestN4U = new RestRequest("login/facebook?access_token=" + TokenFB, Method.GET);
											var requestN4U = new RestRequest("notification/beaconreceive/" + MessaggioApp._key, Method.GET);
											requestN4U.AddHeader("content-type", "application/json");
											requestN4U.AddHeader("Net4U-Company", "roccogiocattoli");
											requestN4U.AddHeader("Net4U-Token", prefs.GetString ("TokenN4U", null));

											client.Execute(requestN4U);

											Descrizione = MessaggioApp.Descrizione;
											ImageUrl = MessaggioApp.UrlImage;
											Key = MessaggioApp._key;


											Console.WriteLine ("Paused:"+Paused+"|"+IsFinishing+"|"+IsDestroyed);

											if (!Paused) {
												var intent = new Intent (this, typeof(NotificationActivity));
												intent.PutExtra ("Beacons", true);
												this.StartActivity (intent);
											} else {
												ShowNotification (MessaggioApp.Titolo);
											}

											Console.WriteLine (MessaggioApp.Titolo + "   " + MessaggioApp.Descrizione);
											w = ListMessaggi.Count + 1;
											a = e.Beacons.Count + 1;
										}
									}
								}
								if (a == e.Beacons.Count + 1)
									break; //esco dal foreach
							}
						}
					}
				}
			}
		}

		#region IBeaconConsumer impl
		public void OnIBeaconServiceConnect()
		{
			Console.WriteLine ("Connesso  Al beacon");

			_iBeaconManager.StartMonitoringBeaconsInRegion(_monitoringRegion);
			_iBeaconManager.StartRangingBeaconsInRegion(_rangingRegion);
		}
		#endregion

		private void ShowNotification(string message)
		{
			var resultIntent = new Intent(this, typeof(NotificationActivity));
			resultIntent.PutExtra ("Beacons",true);
			resultIntent.AddFlags(ActivityFlags.ReorderToFront);
			var pendingIntent = PendingIntent.GetActivity(this, 0, resultIntent, PendingIntentFlags.UpdateCurrent);

			NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
				.SetDefaults((int)(NotificationDefaults.Sound | NotificationDefaults.Vibrate))
				.SetSmallIcon(Resource.Drawable.push_icon)
				.SetContentTitle("RoccoGiocattoli")
				.SetContentText (message)
				.SetContentIntent(pendingIntent)
				.SetAutoCancel(true);

			var notification = builder.Build();

			var notificationManager = (NotificationManager)GetSystemService(NotificationService);
			notificationManager.Notify(0, notification);
		}
			

		protected override void OnDestroy()
		{
			base.OnDestroy();

			_rangeNotifier.DidRangeBeaconsInRegionComplete -= RangingBeaconsInRegion;
			Console.WriteLine ("destroy");
			_iBeaconManager.StopMonitoringBeaconsInRegion(_monitoringRegion);
			_iBeaconManager.StopRangingBeaconsInRegion(_rangingRegion);
			_iBeaconManager.UnBind(this);
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			Console.WriteLine ("ONPAUSE1 "+Paused);
			Paused = true;
			Console.WriteLine ("ONPAUSE2 "+Paused);
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			Console.WriteLine ("ONRESUME1 "+Paused);
			Paused = false;
			Console.WriteLine ("ONRESUME2 "+Paused);

		}

	}



	//************************ CLASSI BEACON ***************************
	public class Beacon{

		public string UUID;
		public int Major;
		public int Minor;
		public int Distance;
		public int IdMessaggio;

		public Beacon(string uuid, int major, int minor, int distance,int idmex){
			UUID = uuid;
			Major = major;
			Minor = minor;
			Distance = distance;
			IdMessaggio = idmex;
		}

	}

	public class Messaggio{

		public string Titolo;
		public string UrlImage;
		public string Descrizione;
		public string Action;
		public bool Visto;
		public string _key;

		public Messaggio(string titolo, string urlimg, string descr, string action,string key,bool visto){
			Titolo= titolo;
			UrlImage = urlimg;
			Descrizione = descr;
			Action = action;
			_key = key;
			Visto = visto;
		}
	}
}


