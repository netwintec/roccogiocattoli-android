﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Views.Animations;


using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace RoccoGiocattoli
{
	

	public class FragmentPuntiVendita : Fragment
	{
		
		public WebView webview;
		public ImageView image;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
				// Use this to return your custom view for this Fragment
			View view = inflater.Inflate(Resource.Layout.FragmentPuntiVendita, container, false);
		
			FragmentPuntiVendita.Instance = this;

			webview = view.FindViewById<WebView> (Resource.Id.webNews);
			webview.Settings.JavaScriptEnabled = true;
			webview.SetWebViewClient (new MyWebViewClient());
			webview.LoadUrl ("http://www.roccogiocattoli.eu/punti-vendita-app/");
			webview.Settings.CacheMode = CacheModes.CacheElseNetwork;

			image = view.FindViewById<ImageView> (Resource.Id.imageView1);

			var rotateAboutCenterAnimation = AnimationUtils.LoadAnimation(Activity.ApplicationContext,Resource.Animation.RotationAnim);
			image.StartAnimation(rotateAboutCenterAnimation);
				
			return view;
		}

		public static FragmentPuntiVendita Instance {get;private set;}
	}

	class MyWebViewClient : WebViewClient
	{
		Boolean loadingFinished = true;
		Boolean redirect = false;

		public MyWebViewClient(){
		}
		public override bool ShouldOverrideUrlLoading(WebView view, string url)
		{
			if (!loadingFinished)
			{
				redirect = true;
			}

			loadingFinished = false;
			view.LoadUrl(url);
			Console.WriteLine("Loading web view...");
			return true;
		}

		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			base.OnPageStarted(view, url, favicon);
			loadingFinished = false;
			Console.WriteLine("Start Loading!!!");
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (!redirect)
			{
				loadingFinished = true;
			}

			if (loadingFinished && !redirect) 
			{
				Console.WriteLine("Finished Loading!!!");
				FragmentPuntiVendita.Instance.webview.LayoutParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent,RelativeLayout.LayoutParams.WrapContent);
				FragmentPuntiVendita.Instance.image.Visibility = ViewStates.Invisible;
			}
			else
			{
				redirect = false;
			}
		}

	}

}

