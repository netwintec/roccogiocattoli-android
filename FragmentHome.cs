﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using RestSharp;

using Android.Content.PM;
using System.Net;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Telephony;

namespace RoccoGiocattoli
{
	public class FragmentHome : Fragment,View.IOnTouchListener,GestureDetector.IOnGestureListener
	{
		List <XmlObject> listXmlObject = new List <XmlObject> ();
		ViewFlipper immagineSwipe;
		protected GestureDetector gestureScanner;
		private static int SWIPE_MIN_DISTANCE = 70;
		private static int SWIPE_MAX_OFF_PATH = 250;
		private static int SWIPE_THRESHOLD_VELOCITY = 150;
		TextView ArticleText;
		int RiferimentoArt=0;

		ImageView RoccoButton,ProfiloButton,NewsButton,PuntiVenditaButton;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			View view;
			TelephonyManager manager = (TelephonyManager)Application.Context.GetSystemService(Context.TelephonyService);
			if(manager.PhoneType == PhoneType.None){
				view = inflater.Inflate(Resource.Layout.FragmentHome_Tablet, container, false);
			}else{
				view = inflater.Inflate(Resource.Layout.FragmentHome, container, false);
			}

			immagineSwipe = view.FindViewById<ViewFlipper> (Resource.Id.immagineSwipe);
			immagineSwipe.SetOnTouchListener (this);

			gestureScanner = new GestureDetector(this);

			if (MainActivity.Instance.XmlObjectDictionary.Count == 0) {
				var client = new RestClient ("http://shop.roccogiocattoli.eu/");
				//client.Authenticator = new HttpBasicAuthenticator(username, password);

				var requestN4U = new RestRequest ("xml/homepage.php", Method.GET);
				requestN4U.AddHeader ("content-type", "application/xml");

				IRestResponse response = client.Execute (requestN4U);

				string content = response.Content; 
				Console.WriteLine (content);
				XmlDocument xmldoc = new XmlDocument ();
				xmldoc.LoadXml (content);
				int i = 0;
				foreach (XmlElement xe in xmldoc.SelectNodes("products/item")) {
			
					string nome = xe.SelectSingleNode ("name").InnerText;
					string url = xe.SelectSingleNode ("url").InnerText;
					string image = xe.SelectSingleNode ("image").InnerText;

					//ImageView imageView = new ImageView (Application.Context);
					//imageView.SetImageResource (Resource.Id.icon);
					//caricaImmagineAsync (image, imageView, i);

					immagineSwipe.AddView(insertPhoto(image,i));
					Console.WriteLine (immagineSwipe.ChildCount);
					Console.WriteLine ("Prodotto:" + nome + "|" + url + "|" + image);
					MainActivity.Instance.XmlObjectDictionary.Add (i, new XmlObject (nome, url, image));
					i++;
				}
				MainActivity.Instance.totalValue=i;
			} else {

				for (int i = 0; i < MainActivity.Instance.totalValue; i++) {
					if (MainActivity.Instance.XmlObjectDictionary [i].imagetBit == null) {
						immagineSwipe.AddView(insertPhoto(MainActivity.Instance.XmlObjectDictionary [i].Image,i));
					} else {
						immagineSwipe.AddView(insertPhotoLoad(i));
					}
				}

			}



			RiferimentoArt=0;

			Typeface Roboto = Typeface.CreateFromAsset(Activity.Assets,"fonts/Roboto_Regular.ttf");
			Typeface Bubblegum = Typeface.CreateFromAsset(Activity.Assets,"fonts/Bubblegum.ttf");

			view.FindViewById<TextView> (Resource.Id.textView1).Typeface=Bubblegum;

			ArticleText = view.FindViewById<TextView> (Resource.Id.ArticleText);
			ArticleText.Typeface = Roboto;
			ArticleText.Text=MainActivity.Instance.XmlObjectDictionary[RiferimentoArt].Nome;

			ImageView Succ = view.FindViewById<ImageView> (Resource.Id.Succ);
			ImageView Prec = view.FindViewById<ImageView> (Resource.Id.Prec);
			Succ.Click+= delegate {
				RiferimentoArt++;
				if(RiferimentoArt==MainActivity.Instance.XmlObjectDictionary.Count)RiferimentoArt=0;
				immagineSwipe.ShowNext();
				ArticleText.Text=MainActivity.Instance.XmlObjectDictionary[RiferimentoArt].Nome;
			};
			Prec.Click+= delegate {
				RiferimentoArt--;
				if(RiferimentoArt<0)RiferimentoArt=MainActivity.Instance.XmlObjectDictionary.Count-1;
				immagineSwipe.ShowPrevious();
				ArticleText.Text=MainActivity.Instance.XmlObjectDictionary[RiferimentoArt].Nome;
			};


			RoccoButton =  view.FindViewById<ImageView>(Resource.Id.RoccoButton);
			RoccoButton.Click += delegate {
				ClickListener(0);
			}; 

			ProfiloButton =  view.FindViewById<ImageView>(Resource.Id.ProfiloButton);
			ProfiloButton.Click += delegate {
				ClickListener(1);
			};

			NewsButton =  view.FindViewById<ImageView>(Resource.Id.NewsButton);
			NewsButton.Click += delegate {
				ClickListener(2);
			};

			PuntiVenditaButton =  view.FindViewById<ImageView>(Resource.Id.NegoziButton);
			PuntiVenditaButton.Click += delegate {
				ClickListener(3);
			};

			return view;
		}
			
		public View insertPhoto(String path,int indice){
			//Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);

			float density = Resources.DisplayMetrics.Density;
			LinearLayout layout = new LinearLayout(Application.Context);
			layout.LayoutParameters = new ViewGroup.LayoutParams ((int)(200*density), (int)(200*density));
			layout.SetGravity (GravityFlags.Center);

			ImageView imageView = new ImageView(Application.Context);
			imageView.LayoutParameters =new ViewGroup.LayoutParams((int)(200*density), (int)(200*density));
			imageView.SetImageResource (Resource.Drawable.placeholder);
			imageView.SetAdjustViewBounds (true);
			imageView.SetScaleType (ImageView.ScaleType.CenterInside);
			imageView.SetOnTouchListener (this);

			imageView.Click += delegate {

			};

			caricaImmagineAsync (path, imageView,indice);
			layout.AddView(imageView);

			return layout;
		}

		public View insertPhotoLoad(int indice){
			//Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);

			float density = Resources.DisplayMetrics.Density;
			LinearLayout layout = new LinearLayout(Application.Context);
			layout.LayoutParameters = new ViewGroup.LayoutParams ((int)(200*density), (int)(200*density));
			layout.SetGravity (GravityFlags.Center);

			ImageView imageView = new ImageView(Application.Context);
			imageView.LayoutParameters =new ViewGroup.LayoutParams((int)(200*density), (int)(200*density));
			imageView.SetImageBitmap (MainActivity.Instance.XmlObjectDictionary[indice].imagetBit);
			imageView.SetAdjustViewBounds (true);
			imageView.SetScaleType (ImageView.ScaleType.CenterInside);
			imageView.SetOnTouchListener (this);

			imageView.Click += delegate {

			};
				
			layout.AddView(imageView);

			return layout;
		}

		async void caricaImmagineAsync(String uri,ImageView immagine_view,int indice){
			WebClient webClient = new WebClient ();
			byte[] bytes = null;
			try{
				bytes = await webClient.DownloadDataTaskAsync(uri);
			}
			catch(TaskCanceledException){
				Console.WriteLine ("Task Canceled!**************************************");
				return;
			}
			catch(Exception e){
				Console.WriteLine (e.ToString());
				return;
			}

			//if (immagine_view.IsInLayout) {

			Bitmap immagine = await BitmapFactory.DecodeByteArrayAsync (bytes, 0, bytes.Length);

			immagine_view.SetImageBitmap (immagine);
			MainActivity.Instance.XmlObjectDictionary [indice].imagetBit = immagine;

			Console.WriteLine ("Immagine caricata!");

		}

		public bool OnTouch (View v, MotionEvent e)
		{
			return gestureScanner.OnTouchEvent(e);
		}

		public bool OnDown (MotionEvent e)
		{
			return true;
		}

		public bool OnFling (MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			Console.WriteLine(e1.GetX()+"  "+e2.GetX()+"  "+ Math.Abs(e1.GetX() - e2.GetX())+"  "+velocityX+"  "+Math.Abs(velocityX));
			try {
				if(e1.GetX() > e2.GetX() && Math.Abs(e1.GetX() - e2.GetX()) > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					Console.WriteLine("L");
					//Toast.makeText(this.getApplicationContext(), "Left", Toast.LENGTH_SHORT).show();
					RiferimentoArt++;
					if(RiferimentoArt==MainActivity.Instance.XmlObjectDictionary.Count)RiferimentoArt=0;
					immagineSwipe.ShowNext();
					ArticleText.Text=MainActivity.Instance.XmlObjectDictionary[RiferimentoArt].Nome;
				}else if (e1.GetX() < e2.GetX() && e2.GetX() - e1.GetX() > SWIPE_MIN_DISTANCE && Math.Abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					Console.WriteLine("R");
					//Toast.makeText(this.getApplicationContext(), "Right", Toast.LENGTH_SHORT).show();
					RiferimentoArt--;
					if(RiferimentoArt<0)RiferimentoArt=MainActivity.Instance.XmlObjectDictionary.Count-1;
					immagineSwipe.ShowPrevious();
					ArticleText.Text=MainActivity.Instance.XmlObjectDictionary[RiferimentoArt].Nome;
				}
			} catch (Exception e) {
				// nothing
			}
			return true;

		}

		public void OnLongPress (MotionEvent e)
		{
		}

		public bool OnScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			return true;
		}

		public void OnShowPress (MotionEvent e)
		{
		}

		public bool OnSingleTapUp (MotionEvent e)
		{
			var uri = Android.Net.Uri.Parse (MainActivity.Instance.XmlObjectDictionary [RiferimentoArt].Url);
			var intent = new Intent (Intent.ActionView, uri);
			Activity.StartActivity (intent);
			return true;
		}

		public void ClickListener(int button) {
			Fragment fragment = null;
			bool isFragment = false;

			switch (button) {
			case 0:
				fragment = new FragmentRocco();
				isFragment = true;
				break;
			case 1:
				fragment = new FragmentProfilo();
				isFragment = true;
				break;
			case 2:
				fragment = new FragmentNews();
				isFragment = true;
				break;
			case 3:
				fragment = new FragmentPuntiVendita();
				isFragment = true;
				break;
			default:
				break;
			}
			if(isFragment) {
				FragmentManager frgManager = this.FragmentManager;
				frgManager.BeginTransaction ().Replace (Resource.Id.content_frame, fragment)
					.AddToBackStack (null)
					.Commit();
			}
		}
	}

}

