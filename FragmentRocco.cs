﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Telephony;

using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;


namespace RoccoGiocattoli
{
	public class FragmentRocco : Fragment
	{

		int i =0;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View view;
			TelephonyManager manager = (TelephonyManager)Application.Context.GetSystemService(Context.TelephonyService);
			if(manager.PhoneType == PhoneType.None){
				view = inflater.Inflate(Resource.Layout.FragmentRocco_Tablet, container, false);
			}else{
				view = inflater.Inflate(Resource.Layout.FragmentRocco, container, false);
			}

			Typeface Roboto = Typeface.CreateFromAsset(Activity.Assets,"fonts/Roboto_Regular.ttf");
			Typeface Bubble = Typeface.CreateFromAsset(Activity.Assets,"fonts/Bubblegum.ttf");

			view.FindViewById<TextView> (Resource.Id.textView1).Typeface = Bubble;
			if (manager.PhoneType != PhoneType.None) {
				TextView Label = view.FindViewById<TextView> (Resource.Id.textView2);
				Label.Typeface = Roboto;
				Label.Click += delegate {
					i++;
					if (i % 2 == 1) {

						view.FindViewById<TextView> (Resource.Id.textView2).Text = "Rocco Giocattoli, fondata nel 1962 dal sig. Rocco D’Alessandris attuale presidente della Società, è una delle storiche realtà nella distribuzione del giocattolo in Italia.\nOggi Rocco Giocattoli è strutturata in quattro unità di business: l’area DISTRIBUZIONE, che grazie alla presenza di oltre 20 agenti commerciali distribuisce in esclusiva prodotti e linee di importanti marchi internazionali sul territorio italiano, oltre alla distribuzione di prodotti di propria ideazione e/o produzione; l’area RETAIL, la rete di punti vendita in parte gestiti direttamente in parte in affiliazione sul territorio laziale; l’area shop-in-shop COIN, UPIM e OVS, la rete di corner specializzati all’interno dei negozi del gruppo Coin, Upim e OVS, tutti  accomunati da una medesima immagine coordinata, e pensati per rispondere al meglio alle aspettative dei consumatori; e l’area E-COMMERCE sia diretta ai consumatori finali (B2C), sia i clienti (B2B), perché consapevoli dell’estrema importanza dei canali di vendita on-line. ";
					}
					if (i % 2 == 0) {

						view.FindViewById<TextView> (Resource.Id.textView2).Text = "Rocco Giocattoli, fondata nel 1962 dal sig. Rocco D’Alessandris attuale presidente della Società, è una delle storiche realtà nella distribuzione del giocattolo in Italia.";

					}
				};
			}
			view.FindViewById<TextView> (Resource.Id.textView3).Typeface = Bubble;
			view.FindViewById<TextView> (Resource.Id.textView4).Text = "Via A. Carruccio 181/183 – 00134 Roma\n(Uscita 24 - Ardeatina - G.R.A.)\nTel. 06/713582 - Fax 06/71350056\nEmail: info@roccogiocattoli.com\nP.IVA 01491171003";
			view.FindViewById<TextView> (Resource.Id.textView4).Typeface = Roboto;
			view.FindViewById<TextView> (Resource.Id.textView5).Typeface = Roboto;

			if (manager.PhoneType != PhoneType.None) {
				ImageView pallini = view.FindViewById<ImageView> (Resource.Id.imageView4);
				pallini.Click += delegate {
					i++;
					if (i % 2 == 1) {

						view.FindViewById<TextView> (Resource.Id.textView2).Text = "Rocco Giocattoli, fondata nel 1962 dal sig. Rocco D’Alessandris attuale presidente della Società, è una delle storiche realtà nella distribuzione del giocattolo in Italia.\nOggi Rocco Giocattoli è strutturata in quattro unità di business: l’area DISTRIBUZIONE, che grazie alla presenza di oltre 20 agenti commerciali distribuisce in esclusiva prodotti e linee di importanti marchi internazionali sul territorio italiano, oltre alla distribuzione di prodotti di propria ideazione e/o produzione; l’area RETAIL, la rete di punti vendita in parte gestiti direttamente in parte in affiliazione sul territorio laziale; l’area shop-in-shop COIN, UPIM e OVS, la rete di corner specializzati all’interno dei negozi del gruppo Coin, Upim e OVS, tutti  accomunati da una medesima immagine coordinata, e pensati per rispondere al meglio alle aspettative dei consumatori; e l’area E-COMMERCE sia diretta ai consumatori finali (B2C), sia i clienti (B2B), perché consapevoli dell’estrema importanza dei canali di vendita on-line. ";
					}
					if (i % 2 == 0) {

						view.FindViewById<TextView> (Resource.Id.textView2).Text = "Rocco Giocattoli, fondata nel 1962 dal sig. Rocco D’Alessandris attuale presidente della Società, è una delle storiche realtà nella distribuzione del giocattolo in Italia.";

					}
				};
			}
			LinearLayout button = view.FindViewById<LinearLayout> (Resource.Id.button);

			button.Click += delegate {
				var email = new Intent (Android.Content.Intent.ActionSend);
				email.PutExtra (Android.Content.Intent.ExtraEmail,
					new string[]{"info@roccogiocattoli.com"} );
				//email.PutExtra (Android.Content.Intent.ExtraSubject, "Hello Email");
				//email.PutExtra (Android.Content.Intent.ExtraText,
				//	"Hello from Xamarin.Android");
				email.SetType ("message/rfc822");
				StartActivity (email);
			};
			return view;
		}
	}
}

